﻿using POP2018.database;
using POP2018.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace POP2018.wpf
{
    /// <summary>
    /// Interaction logic for KupovinaAviokarteWindow.xaml
    /// </summary>
    public partial class KupovinaAviokarteWindow : Window
    {

        private Sediste sediste;
        private bool zauzeto = false;
        private string btn_SEDISTE;
        Let let;
        Karta karta;
        Karta.ETipKlase klasaSedista = Karta.ETipKlase.EKONOMSKA;
        double cena;
        double cenaOdustani;

        public KupovinaAviokarteWindow(Let let)
        {
            InitializeComponent();
            this.let = let;

            tbBrojLeta.DataContext = let;
            lbCenaLeta.DataContext = let;
            cenaOdustani = let.CenaLeta;

            cbKlasa.DataContext = karta;
            cbKlasa.ItemsSource = Enum.GetValues(typeof(Karta.ETipKlase)).Cast<Karta.ETipKlase>();
            btnDalje.IsEnabled = false;
            btnOK.IsEnabled = false;

            if (Aplikacija.UlogovanKorisnik != null)
            {
                if (Aplikacija.UlogovanKorisnik.TipKorisnika == Korisnik.ETipKorisnika.NEPRIJAVLJEN)
                {
                    btnOK.Visibility = Visibility.Collapsed;
                    btnDalje.Visibility = Visibility.Visible;
                }
            }
            else
            {
                btnOK.Visibility = Visibility.Collapsed;
                btnDalje.Visibility = Visibility.Visible;
            }



        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            let.CenaLeta = cenaOdustani;
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {

            if (zauzeto)
            {
                MessageBox.Show("Sediste je zauzeto! ", "Poruka");
                return;
            }
            else
            {
                karta = new Karta();
                karta.BrojLeta = let.Sifra;
                karta.Sediste = btn_SEDISTE;
                karta.Putnik = Aplikacija.UlogovanKorisnik.KorisnickoIme;
                karta.TipKlase = klasaSedista;
                karta.Kapija = "GATE";
                karta.Cena = let.CenaLeta;
                Aplikacija.Instance.Karte.Add(karta);
                Karta.DodajKartu(karta);
                let.CenaLeta = cenaOdustani;

                if (!utility.Povratni.povratni)
                {
                    if (MessageBox.Show("Da li biste zeleli i povratni let?", "Povratni let",
                    MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    {
                        utility.Povratni.povratni = true;
                        LetoviWindow lw = new LetoviWindow(utility.Povratni.povratni, karta.BrojLeta);
                        lw.ShowDialog();
                        this.Close();
                    }
                    
                }
                utility.Povratni.povratni = false;
                this.Close();
                MessageBox.Show("Uspesno ste kupili avio-kartu! ", "Poruka");
                return;
                
                
                

            }




        }

        private void btnDalje_Click(object sender, RoutedEventArgs e)
        {
            if (zauzeto)
            {
                MessageBox.Show("Sediste je zauzeto! ", "Poruka");
                return;
            }
            else
            {
                karta = new Karta();
                karta.BrojLeta = let.Sifra;
                karta.Sediste = btn_SEDISTE;
                karta.TipKlase = klasaSedista;
                karta.Kapija = "GATE";
                karta.Cena = let.CenaLeta;
                let.CenaLeta = cenaOdustani;
                this.Close();
                KupovinaAviokarteWindowPodaciNeulogovanogKorisnika kup = new KupovinaAviokarteWindowPodaciNeulogovanogKorisnika(karta);
                kup.ShowDialog();
            }


        }

        private void cbKlasa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (cbKlasa.SelectedItem.ToString().Equals(Karta.ETipKlase.BIZNIS.ToString()))
            {
                klasaSedista = Karta.ETipKlase.BIZNIS;

                cena = Int32.Parse(lbCenaLeta.Text) * 1.5;
                let.CenaLeta = cena;
                lbCenaLeta.Text = cena.ToString();
            }
            lbCenaLeta.DataContext = let;

            cbKlasa.IsEnabled = false;
            lbDesc1.Visibility = Visibility.Visible;
            DataContextSediste();
            GenerisanjeSedista();


        }

        private void DataContextSediste()
        {
            foreach (Avion av in Aplikacija.Instance.Avioni)
            {
                if (let.Sifra == av.BrojLeta)
                {
                    if (klasaSedista == Karta.ETipKlase.BIZNIS)
                    {
                        sediste = av.SedistaBiznis;
                        lboxSedista.ItemsSource = av.SedistaBiznis.ListaSlobodnihSedista;
                    }
                    else
                    {
                        sediste = av.SedistaEkonomske;
                        lboxSedista.ItemsSource = av.SedistaEkonomske.ListaSlobodnihSedista;
                    }
                }
            }
        }

        private void GenerisanjeSedista()
        {
            int count = 1;
            string all = "0ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            int rows = Int32.Parse(sediste.BrojRedova);
            int columns = Int32.Parse(sediste.BrojKolona);

            int[,] arr = new int[rows, columns];

            int rowLength = arr.GetLength(0);
            int colLength = arr.GetLength(1);

            for (int i = 1; i <= rowLength; i++)
            {
                for (int j = 1; j <= colLength; j++)
                {
                    Button newBtn = new Button();

                    newBtn.Content = i.ToString() + all[count];
                    newBtn.Name = "btn_" + i.ToString() + all[count];
                    newBtn.Height = 30;
                    newBtn.Width = 30;
                    var bc = new BrushConverter();
                    newBtn.Background = (Brush)bc.ConvertFrom("#FF009FF9");
                    newBtn.FontFamily = new FontFamily("Impact");
                    newBtn.Foreground = Brushes.White;
                    newBtn.FontSize = 16;
                    newBtn.Click += new RoutedEventHandler(this.btn_Click);
                    Rootn.Children.Add(newBtn);
                    ++count;
                }
                TextBlock txt = new TextBlock();
                txt.Width = 1000;
                txt.Height = 5;
                Rootn.Children.Add(txt);
                count = 1;
            }
        }

        private void btn_Click(object sender, RoutedEventArgs e)
        {
            
            btnDalje.IsEnabled = true;
            btnOK.IsEnabled = true;
            Button button = sender as Button;
            button.Background = Brushes.Red;
            
            string[] btn_Sediste = button.Name.Split('_');
            btn_SEDISTE = btn_Sediste[1];
            zauzeto = false;
            foreach (Avion av in Aplikacija.Instance.Avioni)
            {
                foreach (Karta kar in Aplikacija.Instance.Karte)
                {
                    if (av.BrojLeta == let.Sifra)
                    {
                        if (kar.BrojLeta.Equals(av.BrojLeta))
                        {
                            if (kar.TipKlase.ToString().Equals(klasaSedista.ToString()))
                            {
                                if (kar.Sediste.Equals(btn_SEDISTE))
                                {

                                    zauzeto = true;
                                }
                            }

                        }
                    }
                }
            }

        }

        
    }
}
