﻿using POP2018.database;
using POP2018.model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace POP2018.wpf
{
    /// <summary>
    /// Interaction logic for AvioniWindowAddNEdit.xaml
    /// </summary>
    public partial class AvioniWindowAddNEdit : Window
    {
        public Avion avion;
        public Aviokompanija avio;
        public enum Stanje { DODAVANJE, IZMENA }
        Stanje stanje;


        public AvioniWindowAddNEdit(Avion avion, Stanje stanje = Stanje.DODAVANJE)
        {
            InitializeComponent();
            this.avion = avion;
            this.stanje = stanje;

            dgSedista.ItemsSource = Aplikacija.Instance.Sedista;
            dgSedista.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);


            if(stanje == Stanje.DODAVANJE)
            {
                lboxAvio.ItemsSource = NazivAviokompanija();
                lboxAvio.DataContext = avio;
            }

            if(stanje == Stanje.IZMENA)
            {
                lboxAvio.ItemsSource = NazivAviokompanijaZaIzmenu();
                lboxAvio.DataContext = avio;
            }
            
            
            lboxBiznis.ItemsSource = avion.SedistaBiznis.ListaSlobodnihSedista;
            lboxBiznis.DataContext = avion.SedistaBiznis;

            lboxEkonomska.ItemsSource = avion.SedistaEkonomske.ListaSlobodnihSedista;
            lboxEkonomska.DataContext = avion.SedistaEkonomske;
            
            //cbSifraLeta.DataContext = avion;

        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            if(lboxBiznis.Items.IsEmpty)
            {
                MessageBox.Show("Biznis klasa ne sme ostati prazna! ", "Greska");
                return;
            }
            else if (lboxEkonomska.Items.IsEmpty)
            {
                MessageBox.Show("Ekonomska klasa ne sme ostati prazna! ", "Greska");
                return;
            }
            else if (lboxAvio.SelectedItem == null)
            {
                MessageBox.Show("Morate selektovati aviokompaniju! ", "Greska");
                return;
            }
            else if(cbSifraLeta.SelectedItem == null)
            {
                MessageBox.Show("Morate selektovati let! ", "Greska");
                return;
            }
            else
            {
                
                if (stanje == Stanje.IZMENA)
                {
                    this.DialogResult = true;
                    avion.BrojLeta = cbSifraLeta.SelectedItem.ToString();
                    avion.NazivAviokompanije = lboxAvio.SelectedItem.ToString();
                    Avion.IzmeniAvion(avion);
                }
                if (stanje == Stanje.DODAVANJE)
                {
                    this.DialogResult = true;
                    avion.BrojLeta = cbSifraLeta.SelectedItem.ToString();
                    avion.NazivAviokompanije = lboxAvio.SelectedItem.ToString();
                    Aplikacija.Instance.Avioni.Add(avion);
                    Avion.DodajAvion(avion);


                }
                this.Close();
            }
        }


        private void btnDodajEkonomsku_Click(object sender, RoutedEventArgs e)
        {
            if (dgSedista.SelectedItem == null)
            {
                MessageBox.Show("Morate selektovati zeljena sedista! ", "Greska");
                return;
            }
            else
            {
                avion.SedistaEkonomske = dgSedista.SelectedItem as Sediste;
                lboxEkonomska.ItemsSource = avion.SedistaEkonomske.ListaSlobodnihSedista;
               
            }
        }

        private void btnDodajBiznis_Click(object sender, RoutedEventArgs e)
        {
            if (dgSedista.SelectedItem == null)
            {
                MessageBox.Show("Morate selektovati zeljena sedista! ", "Greska");
                return;
            }
            else
            {
                avion.SedistaBiznis = dgSedista.SelectedItem as Sediste;
                lboxBiznis.ItemsSource = avion.SedistaBiznis.ListaSlobodnihSedista;
            }
        }

        private void btnIzaberiAvio_Click(object sender, RoutedEventArgs e)
        {
            if (lboxAvio.SelectedItem == null)
            {
                MessageBox.Show("Morate selektovati zeljenu aviokompaniju! ", "Greska");
            }
            else
            {
                ObservableCollection<string> listaSifara = new ObservableCollection<string>();
                avio = new Aviokompanija();

                foreach (Aviokompanija a in Aplikacija.Instance.Aviokompanije)
                {
                    if (lboxAvio.SelectedItem.ToString() == a.Sifra)
                    {
                        avio.ListaLetova = a.ListaLetova;
                    }
                }
                foreach (Let let in avio.ListaLetova)
                {
                    listaSifara.Add(let.Sifra);
                }
                cbSifraLeta.ItemsSource = listaSifara;
                cbSifraLeta.SelectedIndex = 0;
            }
            
            
        }

        private void dgSedista_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if ((string)e.Column.Header == "Zauzeto")
            {
                e.Cancel = true;
            }
            else if ((string)e.Column.Header == "Aktivan")
            {
                e.Cancel = true;
            }
            else if ((string)e.Column.Header == "ListaZauzetihSedista")
            {
                e.Cancel = true;
            }
            else if ((string)e.Column.Header == "ListaSlobodnihSedista")
            {
                e.Cancel = true;
            }
        }

        private void dgEkonomska_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if ((string)e.Column.Header == "Zauzeto")
            {
                e.Cancel = true;
            }
            else if ((string)e.Column.Header == "Aktivan")
            {
                e.Cancel = true;
            }
            else if ((string)e.Column.Header == "ListaZauzetihSedista")
            {
                e.Cancel = true;
            }
            else if ((string)e.Column.Header == "ListaSlobodnihSedista")
            {
                e.Cancel = true;
            }
        }

        private void dgBiznis_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if ((string)e.Column.Header == "Zauzeto")
            {
                e.Cancel = true;
            }
            else if ((string)e.Column.Header == "Aktivan")
            {
                e.Cancel = true;
            }
            else if ((string)e.Column.Header == "ListaZauzetihSedista")
            {
                e.Cancel = true;
            }
            else if ((string)e.Column.Header == "ListaSlobodnihSedista")
            {
                e.Cancel = true;
            }
            else if ((string)e.Column.Header == "Id")
            {
                e.Cancel = true;
            }
        }

        private ObservableCollection<string> NazivAviokompanija()
        {
            ObservableCollection<string> nazivAviokompanija = new ObservableCollection<string>();

            foreach(Aviokompanija avio in Aplikacija.Instance.Aviokompanije)
            {
                
                nazivAviokompanija.Add(avio.Sifra); // sifra mi je naziv aviokompanije u .cs
            }

            return nazivAviokompanija;
        }

        private ObservableCollection<string> NazivAviokompanijaZaIzmenu()
        {

            ObservableCollection<string> aviokompanije = new ObservableCollection<string>();

            foreach(Aviokompanija avio in Aplikacija.Instance.Aviokompanije)
            {
                if (avion.NazivAviokompanije == avio.Sifra)
                {
                    aviokompanije.Add(avio.Sifra);
                }
            }

            return aviokompanije;
        }

        /*
        private bool VecPostojiBrojLeta()
        {
            bool postoji = false;
            foreach(Avion a in Aplikacija.Instance.Avioni)
            {

                if (a.BrojLeta.Contains(cbSifraLeta.SelectedItem.ToString()))
                {
                    postoji = true;
                    return postoji;
                }
                
            }
            return postoji;
        }
        */
        private ObservableCollection<Sediste> SedistaBiznis()
        {
            ObservableCollection<Sediste> sedistaBiznis = new ObservableCollection<Sediste>();

            return sedistaBiznis;
        }
    }
}
