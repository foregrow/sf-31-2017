﻿using POP2018.database;
using POP2018.model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace POP2018.wpf
{
    //d.	Karata – po broju leta, nazivu putnika, odredištu, destinaciji, klasi. Pretragu po vremenu leta omogućiti zadavanjem opsega.

    public partial class KarteWindow : Window
    {
        ICollectionView view;
        public KarteWindow()
        {
            InitializeComponent();
            view = CollectionViewSource.GetDefaultView(Aplikacija.Instance.Karte);
            view.MoveCurrentToPosition(-1);
            view.Filter = CustomFilter;
            dgKarte.ItemsSource = view;
            dgKarte.IsSynchronizedWithCurrentItem = true;

            dgKarte.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Auto);
            if (Aplikacija.UlogovanKorisnik == null || Aplikacija.UlogovanKorisnik.TipKorisnika != Korisnik.ETipKorisnika.ADMINISTRATOR)
            {
                dgKarte.ItemsSource = PregledKarata();
                dgKarte.IsSynchronizedWithCurrentItem = true;
                btnAdd.Visibility = Visibility.Collapsed;
                btnUpdate.Visibility = Visibility.Collapsed;
                btnDelete.Visibility = Visibility.Collapsed;
            }
        }

        private bool CustomFilter(object obj)
        {
            //po broju leta, nazivu putnika, odredištu, destinaciji, klasi. Pretragu po vremenu leta omogućiti zadavanjem opsega.
            Karta karta = obj as Karta;


            if (tbPretragaBrojLeta.Text.Equals(string.Empty) & tbPretragaKlasa.Text.Equals(string.Empty) & tbPretragaDestinacija.Text.Equals(string.Empty) &
                tbPretragaPolaziste.Text.Equals(string.Empty) & tbPretragaPutnik.Text.Equals(string.Empty))
            {
                return karta.Aktivan;
            }

            else
            {
                foreach(Let let in Aplikacija.Instance.Letovi)
                {
                    if (let.Sifra == karta.BrojLeta)
                    {
                        return karta.Aktivan & karta.BrojLeta.Contains(tbPretragaBrojLeta.Text) & karta.TipKlase.ToString().Contains(tbPretragaKlasa.Text)
                            & karta.Putnik.Contains(tbPretragaPutnik.Text) & let.Destinacija.Contains(tbPretragaDestinacija.Text) 
                            & let.Polaziste.Contains(tbPretragaPolaziste.Text);
                    }
                }
                return karta.Aktivan;
            }

        }


        private void dgKarte_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if ((string)e.Column.Header == "Id")
            {
                e.Cancel = true;
            }
            else if ((string)e.Column.Header == "Aktivan")
            {
                e.Cancel = true;
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (view.CurrentPosition == -1)
            {
                MessageBox.Show("Nije selektovana nijedna karta!", "Greska");
                return;
            }
            else
            {
                Karta selektovanaKarta = view.CurrentItem as Karta;
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda",
                    MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    int indeks = IndeksKarta(selektovanaKarta.Id);
                    Aplikacija.Instance.Karte[indeks].Aktivan = false;
                    Karta.ObrisiKartu(selektovanaKarta);
                }
            }
            view.Refresh();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            Karta k = new Karta();
            KarteWindowAddNEdit kw = new KarteWindowAddNEdit(k);
            kw.ShowDialog();
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            Karta selektovanaKarta = view.CurrentItem as Karta;

            if (view.CurrentPosition != -1)
            {
                Karta old = (Karta)selektovanaKarta.Clone();
                KarteWindowAddNEdit kwae = new KarteWindowAddNEdit(selektovanaKarta, KarteWindowAddNEdit.Stanje.IZMENA);
                if (kwae.ShowDialog() != true)
                {
                    int index = IndeksKarta(selektovanaKarta.Id);
                    Aplikacija.Instance.Karte[index] = old;
                }
            }
            else
            {
                MessageBox.Show("Nije selektovana nijedana karta!", "Greska");
                return;
            }

        }

        private int IndeksKarta(int Id)
        {
            int indeks = -1;
            for (int i = 0; i < Aplikacija.Instance.Karte.Count; i++)
            {
                if (Aplikacija.Instance.Karte[i].Id.Equals(Id))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private ObservableCollection<Karta> PregledKarata()
        {
            ObservableCollection<Karta> listaZapregledKarata = new ObservableCollection<Karta>();
            foreach (Karta k in Aplikacija.Instance.Karte)
            {
                if (Aplikacija.UlogovanKorisnik.KorisnickoIme == k.Putnik)
                {
                    listaZapregledKarata.Add(k);
                }
            }
            return listaZapregledKarata;
        }

        private void tbPretragaBrojLeta_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void tbPretragaPutnik_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void tbPretragaPolaziste_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void tbPretragaDestinacija_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void tbPretragaKlasa_KeyUp(object sender, KeyEventArgs e)
        {

        }
    }
}
