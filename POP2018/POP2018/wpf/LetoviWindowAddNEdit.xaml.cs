﻿using POP2018.database;
using POP2018.model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace POP2018.wpf
{
    public partial class LetoviWindowAddNEdit : Window
    {

        public Let Let { get; set; }
        public enum Stanje { DODAVANJE, IZMENA };
        Stanje stanje;

        public LetoviWindowAddNEdit(Let let, Stanje stanje = Stanje.DODAVANJE)
        {
            Aviokompanije();
            InitializeComponent();
            this.Let = let;
            this.stanje = stanje;

            
            tbSifra.DataContext = let;
            tbPolaziste.DataContext = let;
            tbDestinacija.DataContext = let;
            dtpVremeDolaska.DataContext = let;
            dtpVremePolaska.DataContext = let;
            tbCenaLeta.DataContext = let;
            tbPilot.DataContext = let;


            if (stanje == Stanje.IZMENA)
            {
                tbSifra.IsEnabled = false;
            }

            if(stanje == Stanje.DODAVANJE)
            {
                dtpVremePolaska.Value = DateTime.Now;
                dtpVremeDolaska.Value = DateTime.Now;
            }

        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {

            bool postoji = false;
            foreach (Let let in Aplikacija.Instance.Letovi)
            {
                if (tbSifra.GetLineText(0).Equals(let.Sifra))
                {
                    postoji = true;
                }
            }

            List<string> sifreAerodroma = new List<string>();
            foreach (Aerodrom aerodrom in Aplikacija.Instance.Aerodromi)
            {
                sifreAerodroma.Add(aerodrom.Sifra);
            }
            if (stanje == Stanje.DODAVANJE)
            {
                if (tbSifra.GetLineText(0).Length != 3)
                {
                    MessageBox.Show("Sifra leta mora imati tacno 3 broja! ", "Greska");
                    return;
                }
                else if (!tbSifra.GetLineText(0).All(char.IsDigit))
                {
                    MessageBox.Show("Svi karakteri sifre leta moraju biti brojevi! ", "Greska");
                    return;
                }
                else if (postoji)
                {
                    MessageBox.Show("Uneta sifra leta vec postoji! ", "Greska");
                    return;
                }
            }

            if (tbPolaziste.GetLineText(0) == "")
            {
                MessageBox.Show("Morate uneti polaziste leta! ", "Greska");
                return;
            }
            else if(tbPilot.GetLineText(0) == "")
            {
                MessageBox.Show("Morate uneti pilota! ", "Greska");
                return;
            }
            else if (!sifreAerodroma.Contains(tbPolaziste.GetLineText(0)))
            {
                MessageBox.Show("Polaziste je predstavljeno preko sifre aerodroma! ", "Greska");
                return;
            }
            else if (tbDestinacija.GetLineText(0) == "")
            {
                MessageBox.Show("Morate uneti destinaciju leta! ", "Greska");
                return;
            }
            else if (!sifreAerodroma.Contains(tbDestinacija.GetLineText(0)))
            {
                MessageBox.Show("Destinacija je predstavljena preko sifre aerodroma! ", "Greska");
                return;
            }
            else if (tbPolaziste.GetLineText(0).Equals(tbDestinacija.GetLineText(0)))
            {
                MessageBox.Show("Polaziste i destinacija moraju imati razlicite vrednosti! ", "Greska");
                return;
            }
            else if (tbCenaLeta.GetLineText(0) == "")
            {
                MessageBox.Show("Morate uneti cenu leta! ", "Greska");
                return;
            }
            else if (!tbCenaLeta.GetLineText(0).All(char.IsDigit))
            {
                MessageBox.Show("Cena leta mora biti broj! ", "Greska");
                return;
            }
            else
            {
                
                if (stanje == Stanje.DODAVANJE)
                {
                    this.DialogResult = true;
                    Let.Avio = " ";
                    Aplikacija.Instance.Letovi.Add(Let);
                    Let.DodajLet(Let);
                }

                if(stanje == Stanje.IZMENA)
                {
                    this.DialogResult = true;
                    Let.IzmeniLet(Let);
                }
                this.Close();
            }
        }

        private ObservableCollection<string> Aviokompanije()
        {
            ObservableCollection<string> SifreAviokompanija = new ObservableCollection<string>();

            foreach (Aviokompanija avio in Aplikacija.Instance.Aviokompanije)
            {
                SifreAviokompanija.Add(avio.Sifra);
            }

            return SifreAviokompanija;
        }

    }
}
