﻿using POP2018.database;
using POP2018.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace POP2018.wpf
{
    /// <summary>
    /// Interaction logic for SedistaWindow.xaml
    /// </summary>
    public partial class SedistaWindow : Window
    {
        ICollectionView view;
        public SedistaWindow()
        {
            InitializeComponent();
            view = CollectionViewSource.GetDefaultView(Aplikacija.Instance.Sedista);
            view.MoveCurrentToPosition(-1);
            view.Filter = CustomFilter;
            dgSedista.ItemsSource = view;
            dgSedista.IsSynchronizedWithCurrentItem = true;

            dgSedista.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
        }

        private bool CustomFilter(object obj)
        {
            Sediste sediste = obj as Sediste;

            return sediste.Aktivan;

        }


        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (view.CurrentPosition == -1)
            {
                MessageBox.Show("Nije selektovano nista! ", "Greska");
                return;
            }
            else
            {
                Sediste selektovanoSediste = view.CurrentItem as Sediste;
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda",
                    MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    int indeks = IndeksSediste(selektovanoSediste.Id);
                    Aplikacija.Instance.Sedista[indeks].Aktivan = false;
                    Sediste.ObrisiSediste(selektovanoSediste);
                }
            }
            view.Refresh();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            Sediste s = new Sediste();
            SedisteWindowAdd swa = new SedisteWindowAdd(s);
            swa.ShowDialog();
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {

            Sediste selektovanoSediste = view.CurrentItem as Sediste;

            if (view.CurrentPosition != -1)
            {
                Sediste old = (Sediste)selektovanoSediste.Clone();

                SedisteWindowAdd swe = new SedisteWindowAdd(selektovanoSediste, SedisteWindowAdd.Stanje.IZMENA);
                if (swe.ShowDialog() != true)
                {

                    int index = Aplikacija.Instance.Sedista.IndexOf(selektovanoSediste);
                    Aplikacija.Instance.Sedista[index] = old;
                }
            }
            else
            {
                MessageBox.Show("Nista nije selektovano! ", "Greska");
                return;
            }
        }

        private void dgSedista_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if ((string)e.Column.Header == "Aktivan")
            {
                e.Cancel = true;
            }
            else if ((string)e.Column.Header == "ListaSlobodnihSedista")
            {
                e.Cancel = true;
            }
            else if ((string)e.Column.Header == "Id")
            {
                e.Cancel = true;
            }
        }

        private int IndeksSediste(int id)
        {
            int indeks = -1;
            for (int i = 0; i < Aplikacija.Instance.Sedista.Count; i++)
            {
                if (Aplikacija.Instance.Sedista[i].Id.Equals(id))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private void btnPrikaz_Click(object sender, RoutedEventArgs e)
        {
            Sediste selektovanoSediste = view.CurrentItem as Sediste;

            if (view.CurrentPosition != -1)
            {
                SedistaWindowPrikaz swp = new SedistaWindowPrikaz(selektovanoSediste);
                swp.ShowDialog();
            }
            else
            {
                MessageBox.Show("Nista nije selektovano! ", "Greska");
                return;
            }
        }
    }
}
