﻿using POP2018.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace POP2018.wpf
{
    /// <summary>
    /// Interaction logic for SedistaWindowPrikaz.xaml
    /// </summary>
    public partial class SedistaWindowPrikaz : Window
    {
        Sediste sediste;

        public SedistaWindowPrikaz(Sediste sediste)
        {
            InitializeComponent();
            this.sediste = sediste;
            
            lboxSedista.ItemsSource = sediste.ListaSlobodnihSedista;
            lboxSedista.DataContext = sediste;
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
