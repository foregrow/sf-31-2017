﻿using POP2018.database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace POP2018.wpf
{
    /// <summary>
    /// Interaction logic for AdministratorWindow.xaml
    /// </summary>
    public partial class AdministratorWindow : Window
    {
        public AdministratorWindow()
        {
            InitializeComponent();
            lbKorisnik.Text = Aplikacija.UlogovanKorisnik.KorisnickoIme.ToString().ToUpper();
        }

        private void btnKorisnici_Click(object sender, RoutedEventArgs e)
        {
            KorisniciWindow kw = new KorisniciWindow();
            kw.ShowDialog();
        }

        private void btnAerodromi_Click(object sender, RoutedEventArgs e)
        {
            AerodromiWindow aw = new AerodromiWindow();
            aw.ShowDialog();
        }

        private void btnLetovi_Click(object sender, RoutedEventArgs e)
        {
            LetoviWindow lw = new LetoviWindow();
            lw.ShowDialog();
        }

        private void btnAviokompanije_Click(object sender, RoutedEventArgs e)
        {
            AviokompanijeWindow aw = new AviokompanijeWindow();
            aw.ShowDialog();
        }

        private void btnSedista_Click(object sender, RoutedEventArgs e)
        {
            SedistaWindow sw = new SedistaWindow();
            sw.ShowDialog();
        }

        private void btnAvioni_Click(object sender, RoutedEventArgs e)
        {
            AvioniWindow aw = new AvioniWindow();
            aw.ShowDialog();
        }

        private void btnKarte_Click(object sender, RoutedEventArgs e)
        {
            KarteWindow kw = new KarteWindow();
            kw.ShowDialog();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            Aplikacija.UlogovanKorisnik = null;
        }
    }
}
