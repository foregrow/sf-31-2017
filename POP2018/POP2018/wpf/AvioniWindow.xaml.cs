﻿using POP2018.database;
using POP2018.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace POP2018.wpf
{
    /// <summary>
    /// Interaction logic for AvioniWindow.xaml
    /// </summary>
    public partial class AvioniWindow : Window
    {
        ICollectionView view;
        public AvioniWindow()
        {
            InitializeComponent();
            view = CollectionViewSource.GetDefaultView(Aplikacija.Instance.Avioni);
            view.MoveCurrentToPosition(-1);
            view.Filter = CustomFilter;
            dgAvioni.ItemsSource = view;
            dgAvioni.IsSynchronizedWithCurrentItem = true;

            dgAvioni.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            
        }

        private bool CustomFilter(object obj)
        {
            Avion avion = obj as Avion;

            if (tbPretragaBrojLeta.Text.Equals(string.Empty) && tbPretragaAvio.Text.Equals(string.Empty))
            {
                return avion.Aktivan;
            }
            else
            {
                return avion.Aktivan && avion.BrojLeta.Contains(tbPretragaBrojLeta.Text) && avion.NazivAviokompanije.Contains(tbPretragaAvio.Text);
            }

        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            Avion selektovanAvion = view.CurrentItem as Avion;

            if (view.CurrentPosition != -1)
            {
                Avion old = (Avion)selektovanAvion.Clone();
                AvioniWindowAddNEdit awae = new AvioniWindowAddNEdit(selektovanAvion, AvioniWindowAddNEdit.Stanje.IZMENA);
                if (awae.ShowDialog() != true)
                {

                    int index = Aplikacija.Instance.Avioni.IndexOf(selektovanAvion);
                    Aplikacija.Instance.Avioni[index] = old;
                }
            }
            else
            {
                MessageBox.Show("Nije selektovan nijedan avion! ", "Greska");
                return;
            }
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            Avion avion = new Avion();
            AvioniWindowAddNEdit awae = new AvioniWindowAddNEdit(avion);
            awae.ShowDialog();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (view.CurrentPosition == -1)
            {
                MessageBox.Show("Nije selektovan nijedan avion!", "Greska");
                return;
            }
            else
            {
                Avion selektovaniAvion = view.CurrentItem as Avion;
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda",
                    MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    int indeks = IndeksAvion(selektovaniAvion.Id);
                    Aplikacija.Instance.Avioni[indeks].Aktivan = false;
                    Avion.ObrisiAvion(selektovaniAvion);
                }
            }
            view.Refresh();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void dgAvioni_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if ((string)e.Column.Header == "Id")
            {
                e.Cancel = true;
            }
            else if ((string)e.Column.Header == "SedistaBiznis")
            {
                e.Cancel = true;
            }
            else if ((string)e.Column.Header == "SedistaEkonomske")
            {
                e.Cancel = true;
            }
            else if ((string)e.Column.Header == "Aktivan")
            {
                e.Cancel = true;
            }
        }

        private int IndeksAvion(int id)
        {
            int indeks = -1;
            for (int i = 0; i < Aplikacija.Instance.Avioni.Count; i++)
            {
                if (Aplikacija.Instance.Avioni[i].Id.Equals(id))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private void tbPretragaBrojLeta_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void tbPretragaAvio_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }
    }
}
