﻿using POP2018.database;
using POP2018.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace POP2018.wpf
{
    /// <summary>
    /// Interaction logic for KarteWindowAddNEdit.xaml
    /// </summary>
    public partial class KarteWindowAddNEdit : Window
    {
        Karta karta;
        public enum Stanje { DODAVANJE, IZMENA };
        Stanje stanje;
        public KarteWindowAddNEdit(Karta karta, Stanje stanje = Stanje.DODAVANJE)
        {
            InitializeComponent();
            this.karta = karta;
            this.stanje = stanje;

            tbBrojLeta.DataContext = karta;
            tbSediste.DataContext = karta;
            tbKapija.DataContext = karta;
            tbCena.DataContext = karta;

            tbKorisnik.DataContext = karta;

        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            if(tbBrojLeta.Text.Equals(string.Empty)){
                MessageBox.Show("Broj leta ne sme ostati prazan! ", "Greska");
                return;
            }
            else if (!tbBrojLeta.Text.All(char.IsDigit))
            {
                MessageBox.Show("Broj leta mora biti broj! ", "Greska");
                return;
            }
            else if (tbSediste.Text.Equals(string.Empty))
            {
                MessageBox.Show("Sediste ne sme biti prazno! ", "Greska");
                return;
            }
            else if(tbCena.Text.Equals(string.Empty))
            {
                MessageBox.Show("Cena ne sme ostati prazna! ", "Greska");
                return;
            }
            else if (!tbCena.Text.All(char.IsDigit))
            {
                MessageBox.Show("Cena mora biti broj! ", "Greska");
                return;
            }
            else if (btnBiznis.IsChecked == false && btnEkonomska.IsChecked == false)
            {
                MessageBox.Show("Morate cekirati klasu koju zelite! ", "Greska");
                return;
            }
            else if (tbKapija.Text.Equals(string.Empty))
            {
                MessageBox.Show("Kapija ne sme ostati prazna! ", "Greska");
                return;
            }
            else
            {
                if (btnBiznis.IsChecked == true)
                {
                    karta.TipKlase = Karta.ETipKlase.BIZNIS;
                }
                else
                {
                    karta.TipKlase = Karta.ETipKlase.EKONOMSKA;
                }

                if (stanje == Stanje.IZMENA)
                {
                    this.DialogResult = true;
                    Karta.IzmeniKartu(karta);
                }

                if (stanje == Stanje.DODAVANJE)
                {
                    this.DialogResult = true;
                    Aplikacija.Instance.Karte.Add(karta);
                    Karta.DodajKartu(karta);
                }


                this.Close();
            }
            
        }
    }
}
