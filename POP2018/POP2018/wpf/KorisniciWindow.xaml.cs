﻿using POP2018.database;
using POP2018.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace POP2018.wpf
{
    public partial class KorisniciWindow : Window
    {
        ICollectionView view;

        public KorisniciWindow()
        {
            InitializeComponent();

            view = CollectionViewSource.GetDefaultView(Aplikacija.Instance.Korisnici);
            view.MoveCurrentToPosition(-1);
            view.Filter = CustomFilter;
            dgKorisnici.ItemsSource = view;
            dgKorisnici.IsSynchronizedWithCurrentItem = true;

            dgKorisnici.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Auto);
        }

        private bool CustomFilter(object obj)
        {
            Korisnik k = obj as Korisnik;
            if (tbPretragaKorisnicko.Text.Equals(string.Empty) & tbPretragaIme.Text.Equals(string.Empty) & tbPretragaPrezime.Text.Equals(string.Empty) &
                tbPretragaTip.Text.Equals(string.Empty))
            {
                return k.Aktivan;
            }
            
            else
            {
                return k.Aktivan & k.KorisnickoIme.Contains(tbPretragaKorisnicko.Text) & k.Ime.Contains(tbPretragaIme.Text)
                    & k.Prezime.Contains(tbPretragaPrezime.Text) & k.TipKorisnika.ToString().Contains(tbPretragaTip.Text);
            }

        }


        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (view.CurrentPosition == -1)
            {
                MessageBox.Show("Nije selektovan nijedan korisnik!", "Greska");
                return;
            }
            else
            {
                Korisnik selektovaniKorisnik = view.CurrentItem as Korisnik;
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda",
                    MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    int indeks = IndeksKorisnik(selektovaniKorisnik.KorisnickoIme);
                    Aplikacija.Instance.Korisnici[indeks].Aktivan = false;
                    Korisnik.BrisanjeKorisnika(selektovaniKorisnik);
                }
            }
            view.Refresh();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }



        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            Korisnik selektovaniKorisnik = view.CurrentItem as Korisnik;

            if (view.CurrentPosition != -1)
            {
                Korisnik old = (Korisnik)selektovaniKorisnik.Clone();
                KorisniciWindowAddNEdit kwae = new KorisniciWindowAddNEdit(selektovaniKorisnik, KorisniciWindowAddNEdit.Stanje.IZMENA);
                if (kwae.ShowDialog() != true)
                {
                    int index = Aplikacija.Instance.Korisnici.IndexOf(selektovaniKorisnik);
                    Aplikacija.Instance.Korisnici[index] = old;
                }
            }
            else
            {
                MessageBox.Show("Nije selektovan nijedan korisnik!", "Greska");
                return;
            }

        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            Korisnik noviKorisnik = new Korisnik();
            KorisniciWindowAddNEdit kwae = new KorisniciWindowAddNEdit(noviKorisnik);
            kwae.ShowDialog();
        }

        private void dgKorisnici_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if ((string)e.Column.Header == "Pol")
            {
                e.Cancel = true;
            }
            else if ((string)e.Column.Header == "Aktivan")
            {
                e.Cancel = true;
            }
            else if ((string)e.Column.Header == "Id")
            {
                e.Cancel = true;
            }
        }

        private int IndeksKorisnik(string korIme)
        {
            int indeks = -1;
            for (int i = 0; i < Aplikacija.Instance.Korisnici.Count; i++)
            {
                if (Aplikacija.Instance.Korisnici[i].KorisnickoIme.Equals(korIme))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private void tbPretragaKorisnicko_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void tbPretragaIme_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void tbPretragaPrezime_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void tbPretragaTip_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        
    }
}
