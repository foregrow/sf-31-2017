﻿using POP2018.database;
using POP2018.model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace POP2018.wpf
{
    public partial class AviokompanijeWindowAddNEdit : Window
    {
        Aviokompanija aviokompanija;
        public enum Stanje { DODAVANJE, IZMENA };
        Stanje stanje;

        public AviokompanijeWindowAddNEdit(Aviokompanija aviokompanija, Stanje stanje = Stanje.DODAVANJE)
        {
            InitializeComponent();
            this.aviokompanija = aviokompanija;
            this.stanje = stanje;

            tbSifra.DataContext = aviokompanija;
            tbPolaziste.DataContext = aviokompanija;

            cbListaLetova.ItemsSource = MoguciLetovi();
            cbListaLetova.DataContext = aviokompanija;
            cbListaLetova.IsSynchronizedWithCurrentItem = true;

            dgLetovi.ItemsSource = aviokompanija.ListaLetova;
            dgLetovi.IsSynchronizedWithCurrentItem = true;

            dgLetovi.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Auto);

            if (stanje == Stanje.IZMENA)
            {
                cbListaLetova.ItemsSource = ListaLetovaZaAviokompaniju();
                tbSifra.IsEnabled = false;
                tbPolaziste.IsEnabled = false;
            }
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            
            bool postoji = false;
            foreach (Aviokompanija avio in Aplikacija.Instance.Aviokompanije)
            {
                if (tbSifra.GetLineText(0).Equals(avio.Sifra))
                {
                    postoji = true;
                }
            }

            if (stanje == Stanje.DODAVANJE)
            {

                if (tbSifra.GetLineText(0) == "")
                {
                    MessageBox.Show("Morate uneti sifru aviokompanije! ", "Greska");
                    return;
                }
                else if (postoji)
                {
                    MessageBox.Show("Uneta sifra aviokompanije vec postoji! ", "Greska");
                    return;
                }
                else if(tbPolaziste.GetLineText(0) == "")
                {
                    MessageBox.Show("Morate uneti polaziste aviokompanije! ", "Greska");
                    return;
                }
                else if(tbPolaziste.GetLineText(0).Length != 3)
                {
                    MessageBox.Show("Polaziste mora imati tacno 3 slova! ", "Greska");
                    return;
                }
                else if (!tbPolaziste.GetLineText(0).All(char.IsUpper))
                {
                    MessageBox.Show("Sva slova polazista moraju biti velika! (primer: BEG)", "Greska");
                    return;
                }
                else if (!ListaPolazistaZaAviokompaniju().Contains(tbPolaziste.GetLineText(0)))
                {
                    MessageBox.Show("Odabrano polaziste ne postoji!" , "Greska");
                    return;
                }
                else if (ValidnostPolazista())
                {
                    MessageBox.Show("Svi letovi moraju imati isto polaziste! ", "Greska");
                    return;
                }
                else if (!dgLetovi.Items.IsEmpty)
                {
                    if (SelektovanoPolaziste() != tbPolaziste.GetLineText(0))
                    {
                        MessageBox.Show("Polazista moraju da se poklapaju! ", "Greska");
                        return;
                    }
                    else if(!DgListaLetovaIpolaziste())
                    {
                        MessageBox.Show("Polazista moraju da se poklapaju! ", "Greska");
                        return;
                    }
                    
                }


            }
            this.DialogResult = true;
            if (stanje == Stanje.DODAVANJE)
            {
                Aplikacija.Instance.Aviokompanije.Add(aviokompanija);
                Aviokompanija.DodajAviokompaniju(aviokompanija);
            }
            this.Close();

        }

        private void btnDodajLet_Click(object sender, RoutedEventArgs e)
        {
            
            if (dgLetovi.Items.Contains(cbListaLetova.SelectedItem))
            {
                MessageBox.Show("Ne mozete uneti isti let vise puta! ", "Greska");
                return;
            }else if (cbListaLetova.SelectedItem == null)
            {
                MessageBox.Show("Morate selektovati let! ", "Greska");
                return;
            }
            else if (SelektovanoPolaziste() != tbPolaziste.GetLineText(0))
            {
                MessageBox.Show("Polazista moraju da se poklapaju! ", "Greska");
                return;
            }
            else
            {
                
                if (stanje == Stanje.IZMENA)
                {
                    Let l = cbListaLetova.SelectedItem as Let;
                    l.Avio = aviokompanija.Sifra;
                    aviokompanija.ListaLetova.Add(l);
                    Let.IzmeniLet(l);
                    
                }

                if(!tbSifra.Text.Equals(string.Empty) && tbSifra.Text != null)
                {
                    if (stanje == Stanje.DODAVANJE)
                    {
                        Let l = cbListaLetova.SelectedItem as Let;
                        aviokompanija.ListaLetova.Add(l);
                    }
                }
                else
                {
                    MessageBox.Show("Unesite sifru aviokompanije! ");
                    return;
                }
                
                
            }


        }

        private void btnObrisiLet_Click(object sender, RoutedEventArgs e)
        {
            if (dgLetovi.SelectedItem == null)
            {
                MessageBox.Show("Morate selektovati let! ","Greska");
                return;
            }
            Let l = dgLetovi.SelectedItem as Let;
            l.Avio = " ";
            aviokompanija.ListaLetova.Remove(l);
            Let.IzmeniLet(l);
        }

        private void dgLetovi_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if ((string)e.Column.Header == "Aktivan")
            {
                e.Cancel = true;
            }
            else if ((string)e.Column.Header == "Id")
            {
                e.Cancel = true;
            }
        }

        private ObservableCollection<Let> ListaLetovaZaAviokompaniju()
        {
            ObservableCollection<Let> listaLetova = new ObservableCollection<Let>();
            foreach (Let l in Aplikacija.Instance.Letovi)
            {
                
                if (tbPolaziste.Text.Insert(0,aviokompanija.Polaziste) == l.Polaziste)
                {
                    listaLetova.Add(l);
                }

            }
            return listaLetova;
        }

        private ObservableCollection<string> ListaPolazistaZaAviokompaniju()
        {
            ObservableCollection<string> listaPolazista = new ObservableCollection<string>();
            foreach (Let l in Aplikacija.Instance.Letovi)
            {
                listaPolazista.Add(l.Polaziste);
            }
            return listaPolazista;
        }

        private string SelektovanoPolaziste()
        {
            List<object> s = new List<object>();
            var varijabla = cbListaLetova.SelectedItem;
            s.Add(varijabla);
            string bla = "";
            foreach (Let a in s)
            {
                bla = Convert.ToString(a.Polaziste);
            }
            return bla;
        }

        private bool ValidnostPolazista()
        {
            List<string> polazista = new List<string>();
            foreach(Let l in dgLetovi.Items)
            {
                polazista.Add(l.Polaziste);
            }
            if (polazista.Any(o => o != polazista[0]))
            {
                return true;
            }
            return false;

        }

        private bool DgListaLetovaIpolaziste()
        {
            List<string> polazista = new List<string>();
            foreach (Let l in dgLetovi.Items)
            {
                if (l.Avio.Equals(string.Empty))
                {
                    polazista.Add(l.Polaziste);
                }
            }
            if (polazista.Contains(tbPolaziste.GetLineText(0)))
            {
                return true;
            }
            return false;
        }

        private ObservableCollection<Let> MoguciLetovi()
        {
            ObservableCollection<Let> letovi = new ObservableCollection<Let>();
            foreach(Let l in Aplikacija.Instance.Letovi)
            {
                if (l.Avio.Equals(string.Empty))
                {
                    letovi.Add(l);
                }
            }

            return letovi;
        }

    }
}
