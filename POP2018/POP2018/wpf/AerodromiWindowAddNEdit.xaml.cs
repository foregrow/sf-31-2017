﻿using POP2018.database;
using POP2018.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace POP2018.wpf
{
    public partial class AerodromiWindowAddNEdit : Window
    {
        Aerodrom aerodrom;
        public enum Stanje { DODAVANJE, IZMENA };
        Stanje stanje;
        public AerodromiWindowAddNEdit(Aerodrom aerodrom, Stanje stanje = Stanje.DODAVANJE)
        {
            InitializeComponent();
            this.aerodrom = aerodrom;
            this.stanje = stanje;

            tbSifra.DataContext = aerodrom;
            tbNaziv.DataContext = aerodrom;
            tbGrad.DataContext = aerodrom;


            if (stanje == Stanje.IZMENA)
            {
                tbSifra.IsEnabled = false;
            }
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {

            bool postoji = false;
            foreach (Aerodrom aerodrom in Aplikacija.Instance.Aerodromi)
            {
                if (tbSifra.GetLineText(0).Equals(aerodrom.Sifra))
                {
                    postoji = true;
                }
            }
            if (stanje == Stanje.DODAVANJE)
            {
                
                if (tbSifra.GetLineText(0).Length != 3)
                {
                    MessageBox.Show("Sifra aerodroma mora imati tacno 3 slova! ", "Greska");
                    return;
                }
                else if (!tbSifra.GetLineText(0).All(char.IsUpper))
                {
                    MessageBox.Show("Sva slova sifre moraju biti velika! (primer: BEG)", "Greska");
                    return;
                }
                else if (postoji)
                {
                    MessageBox.Show("Uneta sifra aerodroma vec postoji! ", "Greska");
                    return;
                }
            }

            if (tbNaziv.GetLineText(0) == "")
            {
                MessageBox.Show("Morate uneti naziv aerodroma! ", "Greska");
                return;
            }
            else if (tbGrad.GetLineText(0) == "")
            {
                MessageBox.Show("Morate uneti grad aerodroma! ", "Greska");
                return;
            }
            else
            {
                
                if(stanje == Stanje.IZMENA)
                {
                    this.DialogResult = true;
                    Aerodrom.IzmeniAerodrom(aerodrom);

                    
                }
                if(stanje == Stanje.DODAVANJE)
                {
                    this.DialogResult = true;
                    Aplikacija.Instance.Aerodromi.Add(aerodrom);
                    Aerodrom.DodajAerodrom(aerodrom);
                }
                this.Close();
            }

        }

    }
}
