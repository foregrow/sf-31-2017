﻿using POP2018.database;
using POP2018.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace POP2018.wpf
{
    /// <summary>
    /// Interaction logic for KupovinaAviokarteWindowPodaciNeulogovanogKorisnika.xaml
    /// </summary>
    // pol, prezime, ime, email, adresu stanovanja
    public partial class KupovinaAviokarteWindowPodaciNeulogovanogKorisnika : Window
    {
        Karta karta;
        public KupovinaAviokarteWindowPodaciNeulogovanogKorisnika(Karta karta)
        {
            InitializeComponent();
            this.karta = karta;

            
            cbPol.ItemsSource = Enum.GetValues(typeof(Korisnik.EPol)).Cast<Korisnik.EPol>();
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {

            if (tbIme.Text.Equals(string.Empty)) 
            {
                MessageBox.Show("Morate uneti ime! ", "Greska");
                return;
            }
            else if (tbPrezime.Text.Equals(string.Empty))
            {
                MessageBox.Show("Morate uneti prezime! ", "Greska");
                return;
            }
            else if (tbAdresa.Text.Equals(string.Empty))
            {
                MessageBox.Show("Morate uneti adresu! ", "Greska");
                return;
            }
            else if (tbEmail.Text.Equals(string.Empty))
            {
                MessageBox.Show("Morate uneti email! ", "Greska");
                return;
            }
            else if (!KorisniciWindowAddNEdit.IsValidEmail(tbEmail.Text))
            {
                MessageBox.Show("Email mora biti u ispravnom formatu! ", "Greska");
                return;
            }
            else if(cbPol.SelectedItem == null)
            {
                MessageBox.Show("Morate selektovati pol! ", "Greska");
                return;
            }
            else
            {
                if (!utility.Povratni.povratni)
                {
                    if (MessageBox.Show("Da li biste zeleli i povratni let?", "Povratni let",
                        MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    {
                        karta.Putnik = tbIme.Text + " " + tbPrezime.Text;
                        Aplikacija.Instance.Karte.Add(karta);
                        Karta.DodajKartu(karta);
                        utility.Povratni.povratni = true;
                        LetoviWindow lw = new LetoviWindow(utility.Povratni.povratni, karta.BrojLeta);
                        lw.ShowDialog();
                        this.Close();
                    }

                }
                utility.Povratni.povratni = false;
                karta.Putnik = tbIme.Text + " " + tbPrezime.Text;
                Aplikacija.Instance.Karte.Add(karta);
                Karta.DodajKartu(karta);
                this.Close();
                MessageBox.Show("Uspesno ste kupili avio-kartu! ", "Poruka");
                return;
            }
            
        }

    }
}
