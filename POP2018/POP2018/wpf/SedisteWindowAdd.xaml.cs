﻿using POP2018.database;
using POP2018.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace POP2018.wpf
{
    /// <summary>
    /// Interaction logic for SedisteWindowAdd.xaml
    /// </summary>
    public partial class SedisteWindowAdd : Window
    {
        Sediste sediste;
        public enum Stanje { DODAVANJE, IZMENA }
        Stanje stanje;
        public SedisteWindowAdd(Sediste sediste, Stanje stanje = Stanje.DODAVANJE)
        {
            InitializeComponent();
            this.sediste = sediste;
            this.stanje = stanje;

            tbBrReda.DataContext = sediste;
            tbBrKolona.DataContext = sediste;
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            if (tbBrReda.Text == "")
            {
                MessageBox.Show("Niste uneli broj redova! ");
                return;
            }
            else if(tbBrKolona.Text == "")
            {
                MessageBox.Show("Niste uneli broj kolona! ");
                return;
            }
            else if (!tbBrReda.Text.All(char.IsDigit) || !tbBrKolona.Text.All(char.IsDigit))
            {
                MessageBox.Show("Broj redova i broj kolona moraju biti brojevi! ");
                return;
            }
            else if (Int32.Parse(tbBrReda.Text) < Int32.Parse(tbBrKolona.Text))
            {
                MessageBox.Show("Broj redova ne sme biti manji od broja kolona! ");
                return;
            }
            else if (Int32.Parse(tbBrKolona.Text) > 26)
            {
                MessageBox.Show("Broj kolona ne moze biti veci od 26");
                return;
            }
            else
            {
                if (stanje == Stanje.IZMENA)
                {
                    this.DialogResult = true;
                    sediste.ListaSlobodnihSedista.Clear();
                    sediste.GenerisanjeSedista();
                    Sediste.IzmeniSediste(sediste);
                }
                
                if (stanje == Stanje.DODAVANJE)
                {
                    this.DialogResult = true;
                    sediste.GenerisanjeSedista();
                    Aplikacija.Instance.Sedista.Add(sediste);
                    Sediste.DodajSediste(sediste);
                }

                this.Close();
            }
        }      

    }
}
