﻿using POP2018.database;
using POP2018.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace POP2018.wpf
{

    public partial class PutnikWindow : Window
    {
        public PutnikWindow()
        {
            InitializeComponent();
            lbKorisnik.Text = Aplikacija.UlogovanKorisnik.KorisnickoIme.ToString().ToUpper();
            
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            Aplikacija.UlogovanKorisnik = null;
        }

        private void btnPregledProfila_Click(object sender, RoutedEventArgs e)
        {
            PutnikWindowPregledNIzmena pwpi = new PutnikWindowPregledNIzmena(Aplikacija.UlogovanKorisnik);
            pwpi.ShowDialog();
        }
        
        private void btnIzmenaProfila_Click(object sender, RoutedEventArgs e)
        {
            Korisnik old = (Korisnik) Aplikacija.UlogovanKorisnik.Clone();

            PutnikWindowPregledNIzmena kwae = new PutnikWindowPregledNIzmena(Aplikacija.UlogovanKorisnik, PutnikWindowPregledNIzmena.Stanje.IZMENA);
            if (kwae.ShowDialog() != true)
            {
                int index = IndeksKorisnik(Aplikacija.UlogovanKorisnik.KorisnickoIme);
                Aplikacija.Instance.Korisnici[index] = old;
            }
        }

        private void btnPregledKupljenihKarti_Click(object sender, RoutedEventArgs e)
        {
            KarteWindow kw = new KarteWindow();
            kw.ShowDialog();
        }

        private void btnPregledLetova_Click(object sender, RoutedEventArgs e)
        {
            LetoviWindow lw = new LetoviWindow();
            lw.ShowDialog();
        }

        private int IndeksKorisnik(string korIme)
        {
            int indeks = -1;
            for (int i = 0; i < Aplikacija.Instance.Korisnici.Count; i++)
            {
                if (Aplikacija.Instance.Korisnici[i].KorisnickoIme.Equals(korIme))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

    }
}
