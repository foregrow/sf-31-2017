﻿using POP2018.database;
using POP2018.model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Globalization;

namespace POP2018.wpf
{
    public partial class LetoviWindow : Window
    {
        ICollectionView view;
        string brLet;

        public LetoviWindow(bool povratni = false, string brLet = "")
        {
            InitializeComponent();
            this.brLet = brLet;
            view = CollectionViewSource.GetDefaultView(Aplikacija.Instance.Letovi);
            view.MoveCurrentToPosition(-1);
            view.Filter = CustomFilter;
            dgLetovi.ItemsSource = view;
            dgLetovi.IsSynchronizedWithCurrentItem = true;
            

            if (povratni && !brLet.Equals(""))
            {
                view = CollectionViewSource.GetDefaultView(ListaLetovaZaPovratni());
                view.MoveCurrentToPosition(-1);
                view.Filter = CustomFilter;
                dgLetovi.ItemsSource = view;
                dgLetovi.IsSynchronizedWithCurrentItem = true;
            }

            dgLetovi.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Auto);
            if (Aplikacija.UlogovanKorisnik == null || Aplikacija.UlogovanKorisnik.TipKorisnika != Korisnik.ETipKorisnika.ADMINISTRATOR)
            {
                Thickness margin = btnKupi.Margin;
                margin.Left = 12;
                margin.Top = 0;
                margin.Right = 0;
                margin.Bottom = 12;
                btnKupi.Margin = margin;

                btnAdd.Visibility = Visibility.Collapsed;
                btnUpdate.Visibility = Visibility.Collapsed;
                btnDelete.Visibility = Visibility.Collapsed;
            }
        }

        private bool CustomFilter(object obj)
        {
            Let l = obj as Let;
            if (tbPretragaSifraLeta.Text.Equals(string.Empty) && tbPretragaAvio.Text.Equals(string.Empty) && tbPretragaPolaziste.Text.Equals(string.Empty) &&
                tbPretragaDestinacija.Text.Equals(string.Empty) && tbPretragaCenaOd.Text.Equals(string.Empty) && tbPretragaCenaDo.Text.Equals(string.Empty) &&
                dtpOd.Value.Equals(null) && dtpDo.Value.Equals(null))
            {
                return l.Aktivan;
            }
            else
            {
                int a;
                Int32.TryParse((tbPretragaCenaOd.Text.Equals(string.Empty) ? int.MinValue.ToString() : tbPretragaCenaOd.Text ), out a);
                int b;
                Int32.TryParse((tbPretragaCenaDo.Text.Equals(string.Empty) ? int.MaxValue.ToString() : tbPretragaCenaDo.Text), out b);

                DateTime odT = dtpOd.Value ?? DateTime.MinValue;
                DateTime doT = dtpDo.Value ?? DateTime.MaxValue;

                return l.Aktivan && l.Sifra.Contains(tbPretragaSifraLeta.Text) && l.Avio.Contains(tbPretragaAvio.Text)
                    && l.Polaziste.Contains(tbPretragaPolaziste.Text) && l.Destinacija.Contains(tbPretragaDestinacija.Text)
                    && a <= l.CenaLeta && b >= l.CenaLeta && odT <= l.VremePolaska && doT >= l.VremeDolaska;
            }

        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (view.CurrentPosition == -1)
            {
                MessageBox.Show("Nije selektovan nijedan let!", "Greska");
            }
            else
            {
                Let selektovaniLet = view.CurrentItem as Let;
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda",
                    MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    int indeks = IndeksLeta(selektovaniLet.Sifra);
                    Aplikacija.Instance.Letovi[indeks].Aktivan = false;
                    Let.BrisanjeLeta(selektovaniLet);
                }
            }
            view.Refresh();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            Let noviLet = new Let();
            LetoviWindowAddNEdit lwae = new LetoviWindowAddNEdit(noviLet);
            lwae.ShowDialog();
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            Let selektovaniLet = view.CurrentItem as Let;

            if (view.CurrentPosition != -1)
            {
                Let old = (Let)selektovaniLet.Clone();
                LetoviWindowAddNEdit kwae = new LetoviWindowAddNEdit(selektovaniLet, LetoviWindowAddNEdit.Stanje.IZMENA);
                if (kwae.ShowDialog() != true)
                {
                    int indeks = IndeksLeta(selektovaniLet.Sifra);
                    Aplikacija.Instance.Letovi[indeks] = old;
                }
            }
            else
            {
                MessageBox.Show("Nije selektovan nijedan let!", "Greska");
                return;
            }
            dgLetovi.Items.Refresh();
        }

        private void dgLetovi_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if ((string)e.Column.Header == "Aktivan")
            {
                e.Cancel = true;
            }
            else if ((string)e.Column.Header == "Id")
            {
                e.Cancel = true;
            }
        }

        private int IndeksLeta(string sifra)
        {
            int indeks = -1;
            for (int i = 0; i < Aplikacija.Instance.Letovi.Count; i++)
            {
                if (Aplikacija.Instance.Letovi[i].Sifra.Equals(sifra))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private void btnKupi_Click(object sender, RoutedEventArgs e)
        {
            if (view.CurrentPosition == -1)
            {
                MessageBox.Show("Morate izabrati let!", "Greska");
            }
            else
            {
                this.Close();
                KupovinaAviokarteWindow kaw = new KupovinaAviokarteWindow(view.CurrentItem as Let);
                kaw.ShowDialog();
            }
            

        }

        private void tbPretragaAvio_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }


        private void tbPretragaPolaziste_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void tbPretragaDestinacija_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void tbPretragaSifraLeta_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void tbPretragaCenaDo_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void tbPretragaCenaOd_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void tbPretragaDatumOd_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void tbPretragaDatumDo_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private ObservableCollection<Let> ListaLetovaZaPovratni()
        {
            ObservableCollection<Let> listaLetova = new ObservableCollection<Let>();

            string avio = ""; 

            foreach(Let let in Aplikacija.Instance.Letovi)
            {
                if(brLet == let.Sifra)
                {
                    avio = let.Avio;
                }
            }

            foreach(Let let in Aplikacija.Instance.Letovi)
            {
                if (let.Avio == avio)
                {
                    listaLetova.Add(let);
                }
            }

            return listaLetova;
        }


    }
}
