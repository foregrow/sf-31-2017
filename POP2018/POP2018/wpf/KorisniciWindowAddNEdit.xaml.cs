﻿using POP2018.database;
using POP2018.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace POP2018.wpf
{
    public partial class KorisniciWindowAddNEdit : Window
    {
        Korisnik korisnik;
        public enum Stanje { DODAVANJE, IZMENA };
        Stanje stanje;


        public KorisniciWindowAddNEdit(Korisnik korisnik, Stanje stanje = Stanje.DODAVANJE)
        {
            InitializeComponent();
            this.korisnik = korisnik;
            this.stanje = stanje;

            cbPol.ItemsSource = Enum.GetValues(typeof(Korisnik.EPol)).Cast<Korisnik.EPol>();
            cbTipKorisnika.ItemsSource = Enum.GetValues(typeof(Korisnik.ETipKorisnika)).Cast<Korisnik.ETipKorisnika>();

            
            tbKorisnickoIme.DataContext = korisnik;
            tbLozinka.DataContext = korisnik;
            tbIme.DataContext = korisnik;
            tbPrezime.DataContext = korisnik;
            tbAdresa.DataContext = korisnik;
            tbEmail.DataContext = korisnik;
            cbPol.DataContext = korisnik;
            cbTipKorisnika.DataContext = korisnik;

            if(stanje == Stanje.IZMENA)
            {
                tbKorisnickoIme.IsEnabled = false;
            }

        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {

            bool postoji = false;
            foreach (Korisnik korisnik in Aplikacija.Instance.Korisnici)
            {
                if (tbKorisnickoIme.GetLineText(0).Equals(korisnik.KorisnickoIme))
                {
                    postoji = true;
                }
            }

            if (stanje == Stanje.DODAVANJE)
            {
                if (postoji)
                {
                    MessageBox.Show("Uneto korisnicko ime vec postoji! ", "Greska");
                    return;
                }
            }

            if (tbKorisnickoIme.GetLineText(0) == "")
            {
                MessageBox.Show("Morate uneti korisnicko ime! ", "Greska");
                return;
            }
            else if (tbLozinka.GetLineText(0) == "")
            {
                MessageBox.Show("Morate uneti lozinku! ", "Greska");
                return;
            }
            else if (tbLozinka.GetLineText(0).Length < 6)
            {
                MessageBox.Show("Lozinka mora imati vise od 5 karaktera! ", "Greska");
                return;
            }
            else if (tbIme.GetLineText(0) == "")
            {
                MessageBox.Show("Morate uneti ime! ", "Greska");
                return;
            }
            else if (tbPrezime.GetLineText(0) == "")
            {
                MessageBox.Show("Morate uneti prezime! ", "Greska");
                return;
            }
            else if (tbAdresa.GetLineText(0) == "")
            {
                MessageBox.Show("Morate uneti adresu! ", "Greska");
                return;
            }
            else if (tbEmail.GetLineText(0) == "")
            {
                MessageBox.Show("Morate uneti email! ", "Greska");
                return;
            }
            else if (!IsValidEmail(tbEmail.GetLineText(0)))
            {
                MessageBox.Show("Email mora biti u ispravnom formatu! ", "Greska");
                return;
            }
            else
            {
                if (stanje == Stanje.IZMENA)
                {
                    this.DialogResult = true;
                    Korisnik.IzmeniKorisnika(korisnik);
                    
                }

                if (stanje == Stanje.DODAVANJE)
                {
                    this.DialogResult = true;
                    Aplikacija.Instance.Korisnici.Add(korisnik);
                    Korisnik.DodajKorisnika(korisnik);
                }
                this.Close();
            }

        }

        public static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }
}
