﻿using POP2018.database;
using POP2018.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace POP2018.wpf
{
    public partial class AviokompanijeWindow : Window
    {
        ICollectionView view;
        public AviokompanijeWindow()
        {
            InitializeComponent();
            view = CollectionViewSource.GetDefaultView(Aplikacija.Instance.Aviokompanije);
            view.MoveCurrentToPosition(-1);
            view.Filter = CustomFilter;
            dgAviokompanije.ItemsSource = view;
            dgAviokompanije.IsSynchronizedWithCurrentItem = true;

            dgAviokompanije.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
        }

        private bool CustomFilter(object obj)
        {
            Aviokompanija av = obj as Aviokompanija;
            return av.Aktivan;
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (view.CurrentPosition == -1)
            {
                MessageBox.Show("Nije selektovan nijedna aviokompanija!", "Greska");
                return;
            }
            else
            {
                Aviokompanija selektovanaAvio = view.CurrentItem as Aviokompanija;
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda",
                    MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    int indeks = IndeksAvio(selektovanaAvio.Sifra);
                    Aplikacija.Instance.Aviokompanije[indeks].Aktivan = false;
                    Aviokompanija.ObrisiAviokompaniju(selektovanaAvio);
                    KadaSeObriseAviokompanija();
                    
                }
            }
            view.Refresh();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            Aviokompanija novaAviokompanija = new Aviokompanija();
            AviokompanijeWindowAddNEdit awae = new AviokompanijeWindowAddNEdit(novaAviokompanija);
            awae.ShowDialog();
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            Aviokompanija selektovanaAviokompanija = view.CurrentItem as Aviokompanija;

            if (view.CurrentPosition != -1)
            {
                Aviokompanija old = (Aviokompanija)selektovanaAviokompanija.Clone();
                AviokompanijeWindowAddNEdit few = new AviokompanijeWindowAddNEdit(selektovanaAviokompanija, AviokompanijeWindowAddNEdit.Stanje.IZMENA);
                if (few.ShowDialog() != true)
                {

                    int index = Aplikacija.Instance.Aviokompanije.IndexOf(selektovanaAviokompanija);
                    Aplikacija.Instance.Aviokompanije[index] = old;
                }
            }
            else
            {
                MessageBox.Show("Nije selektovana nijedna aviokompanija! ", "Greska");
                return;
            }
        }

        private void dgAviokompanije_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if ((string)e.Column.Header == "Aktivan")
            {
                e.Cancel = true;
            }
            else if ((string)e.Column.Header == "ListaLetova")
            {
                e.Cancel = true;
            }
            else if ((string)e.Column.Header == "Id")
            {
                e.Cancel = true;
            }
        }

        private int IndeksAvio(string sifra)
        {
            int indeks = -1;
            for (int i = 0; i < Aplikacija.Instance.Aviokompanije.Count; i++)
            {
                if (Aplikacija.Instance.Aviokompanije[i].Sifra.Equals(sifra))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private void KadaSeObriseAviokompanija()
        {
            foreach (Aviokompanija avio in Aplikacija.Instance.Aviokompanije)
            {
                foreach (Let let in Aplikacija.Instance.Letovi)
                {
                    //kada se obrise aviokompanija svi letovi koji su imali tu aviokompaniju treba da im se property avio promeni u ""
                    if (avio.Aktivan == false)
                    {
                        if (avio.Sifra == let.Avio)
                        {
                            let.Avio = " ";
                            Let.IzmeniLet(let);
                        }
                    }
                }
            }
            
        }
    }
}
