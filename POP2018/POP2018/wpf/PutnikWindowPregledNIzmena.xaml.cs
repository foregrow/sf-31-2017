﻿using POP2018.database;
using POP2018.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace POP2018.wpf
{
    /// <summary>
    /// Interaction logic for PutnikWindowPregledNIzmena.xaml
    /// </summary>
    public partial class PutnikWindowPregledNIzmena : Window
    {
        public enum Stanje { PREGLED, IZMENA };
        Stanje stanje;
        Korisnik korisnik;
        public PutnikWindowPregledNIzmena(Korisnik korisnik, Stanje stanje = Stanje.PREGLED)
        {
            InitializeComponent();
            this.korisnik = korisnik;
            this.stanje = stanje;

            cbPol.ItemsSource = Enum.GetValues(typeof(Korisnik.EPol)).Cast<Korisnik.EPol>();
            cbTipKorisnika.ItemsSource = Enum.GetValues(typeof(Korisnik.ETipKorisnika)).Cast<Korisnik.ETipKorisnika>();

            tbKorisnickoIme.DataContext = korisnik;
            tbLozinka.DataContext = korisnik;
            tbIme.DataContext = korisnik;
            tbPrezime.DataContext = korisnik;
            tbAdresa.DataContext = korisnik;
            tbEmail.DataContext = korisnik;
            cbPol.DataContext = korisnik;
            cbTipKorisnika.DataContext = korisnik;
            cbTipKorisnika.IsEnabled = false;

            if (stanje == Stanje.PREGLED)
            {
                btnOK.Visibility = Visibility.Collapsed;
                btnCancel.Visibility = Visibility.Collapsed;
                tbKorisnickoIme.IsEnabled = false;
                tbLozinka.IsEnabled = false;
                tbIme.IsEnabled = false;
                tbPrezime.IsEnabled = false;
                tbEmail.IsEnabled = false;
                tbAdresa.IsEnabled = false;
                cbPol.IsEnabled = false;
            }
            if(stanje == Stanje.IZMENA)
            {
                btnZatvori.Visibility = Visibility.Collapsed;
                tbKorisnickoIme.IsEnabled = false;
            }

        }

        private void btnZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            if (tbLozinka.GetLineText(0) == "")
            {
                MessageBox.Show("Morate uneti lozinku! ", "Greska");
                return;
            }
            else if (tbLozinka.GetLineText(0).Length < 6)
            {
                MessageBox.Show("Lozinka mora imati vise od 5 karaktera! ", "Greska");
                return;
            }
            else if (tbIme.GetLineText(0) == "")
            {
                MessageBox.Show("Morate uneti ime! ", "Greska");
                return;
            }
            else if (tbPrezime.GetLineText(0) == "")
            {
                MessageBox.Show("Morate uneti prezime! ", "Greska");
                return;
            }
            else if (tbAdresa.GetLineText(0) == "")
            {
                MessageBox.Show("Morate uneti adresu! ", "Greska");
                return;
            }
            else if (tbEmail.GetLineText(0) == "")
            {
                MessageBox.Show("Morate uneti email! ", "Greska");
                return;
            }
            else if (!KorisniciWindowAddNEdit.IsValidEmail(tbEmail.GetLineText(0)))
            {
                MessageBox.Show("Email mora biti u ispravnom formatu! ", "Greska");
                return;
            }
            else
            {
                this.DialogResult = true;
                if (stanje == Stanje.IZMENA)
                {
                    Korisnik.IzmeniKorisnika(korisnik);
                }
                this.Close();
            }
            
        }
    }
}
