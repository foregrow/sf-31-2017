﻿using POP2018.database;
using POP2018.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace POP2018.wpf
{
    public partial class AerodromiWindow : Window
    {
        ICollectionView view;
        public AerodromiWindow()
        {
            InitializeComponent();

            view = CollectionViewSource.GetDefaultView(Aplikacija.Instance.Aerodromi);
            view.MoveCurrentToPosition(-1);
            view.Filter = CustomFilter;
            dgAerodromi.ItemsSource = view;
            dgAerodromi.IsSynchronizedWithCurrentItem = true;

            dgAerodromi.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
        }

        private bool CustomFilter(object obj)
        {
            Aerodrom aerodrom = obj as Aerodrom;

            return aerodrom.Aktivan;

        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (view.CurrentPosition == -1)
            {
                MessageBox.Show("Nije selektovan nijedan aerodrom!", "Greska");
                return;
            }
            else
            {
                Aerodrom selektovaniAerodrom = view.CurrentItem as Aerodrom;
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda",
                    MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    int indeks = IndeksAerodrom(selektovaniAerodrom.Sifra);
                    Aplikacija.Instance.Aerodromi[indeks].Aktivan = false;
                    Aerodrom.BrisanjeAerodroma(selektovaniAerodrom);
                }
            }
            view.Refresh();
        }

            private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            Aerodrom noviAerodrom = new Aerodrom();
            AerodromiWindowAddNEdit kwae = new AerodromiWindowAddNEdit(noviAerodrom);
            kwae.ShowDialog();
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            Aerodrom selektovaniAerodrom = view.CurrentItem as Aerodrom;

            if (view.CurrentPosition != -1)
            {
                Aerodrom old = (Aerodrom)selektovaniAerodrom.Clone();
                AerodromiWindowAddNEdit kwae = new AerodromiWindowAddNEdit(selektovaniAerodrom, AerodromiWindowAddNEdit.Stanje.IZMENA);
                if (kwae.ShowDialog() != true)
                {
                    int index = Aplikacija.Instance.Aerodromi.IndexOf(selektovaniAerodrom);
                    Aplikacija.Instance.Aerodromi[index] = old;
                }
            }
            else
            {
                MessageBox.Show("Nije selektovan nijedan aerodrom!", "Greska");
                return;
            }
            dgAerodromi.Items.Refresh();
        }

        private void dgAerodromi_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if ((string)e.Column.Header == "Aktivan")
            {
                e.Cancel = true;
            }
            else if ((string)e.Column.Header == "Id")
            {
                e.Cancel = true;
            }
        }

        private int IndeksAerodrom(string sifra)
        {
            int indeks = -1;
            for (int i = 0; i < Aplikacija.Instance.Aerodromi.Count; i++)
            {
                if (Aplikacija.Instance.Aerodromi[i].Sifra.Equals(sifra))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private void tbSearch_KeyUp(object sender, KeyEventArgs e)
        {
            
        }
    }
}
