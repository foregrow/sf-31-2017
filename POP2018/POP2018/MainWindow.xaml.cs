﻿using POP2018.wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace POP2018
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            model.Korisnik.UcitajKorisnike();
            model.Aerodrom.UcitajAerodrome();
            model.Let.UcitajLetove();
            model.Aviokompanija.UcitajAviokompanije();
            model.Sediste.UcitajSedista();
            model.Avion.UcitajAvione();
            model.Karta.UcitajKarte();
        }


        private void btnPrijava_Click(object sender, RoutedEventArgs e)
        {
            if (tbUsername.Text == "")
            {
                MessageBox.Show("Morate uneti korisnicko ime! ", "Greska");
                return;
            }
            else if(tbPassword.Password == "")
            {
                MessageBox.Show("Morate uneti lozinku! ", "Greska");
                return;
            }
            else if (PrijavaKorisnika())
            {
                MessageBox.Show("Korisnicko ime ili lozinka je neispravna! ", "Greska");
                return;
            }
            else
            {
                if(RoleKorisnika() == "ADMINISTRATOR")
                {
                    database.Aplikacija.UlogovanKorisnik = UlogovanKorisnik();
                    AdministratorWindow aw = new AdministratorWindow();
                    aw.ShowDialog();
                    tbUsername.Text = "";
                    tbPassword.Password = "";
                }
                else if (RoleKorisnika() == "PUTNIK")
                {
                    database.Aplikacija.UlogovanKorisnik = UlogovanKorisnik();
                    PutnikWindow pw = new PutnikWindow();
                    pw.ShowDialog();
                    tbUsername.Text = "";
                    tbPassword.Password = "";
                }
                else
                {
                    MessageBox.Show("Nepostojeci role korisnika! ", "Greska");
                    return;

                }
            }
            
        }

        private void btnNeregistrovan_Click(object sender, RoutedEventArgs e)
        {
            LetoviWindow lw = new LetoviWindow();
            lw.ShowDialog();
;
        }


        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private bool PrijavaKorisnika()
        {
            
            bool neispravno = true;
            foreach (model.Korisnik korisnik in database.Aplikacija.Instance.Korisnici)
            {
                if(korisnik.KorisnickoIme == tbUsername.Text && korisnik.Lozinka == tbPassword.Password)
                {
                    neispravno = false;
                }
            }
            return neispravno;
        }

        private string RoleKorisnika()
        {
            
            string role = "";
            foreach (model.Korisnik korisnik in database.Aplikacija.Instance.Korisnici)
            {
                if(tbUsername.Text == korisnik.KorisnickoIme && tbPassword.Password == korisnik.Lozinka)
                {
                    role = korisnik.TipKorisnika.ToString();
                }
            }
            return role;
        }

        public model.Korisnik UlogovanKorisnik()
        {
            model.Korisnik k = new model.Korisnik();
            foreach(model.Korisnik korisnik in database.Aplikacija.Instance.Korisnici)
            {
                if(korisnik.KorisnickoIme == tbUsername.Text && korisnik.Lozinka == tbPassword.Password)
                {
                    k = korisnik;
                }
            }

            return k;
        }
    }
}
