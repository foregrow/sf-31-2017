﻿#pragma checksum "..\..\..\wpf\AdministratorWindow.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "5963EA8F60D79E35072FAF79516500EBA8CD908B"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using POP2018.wpf;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace POP2018.wpf {
    
    
    /// <summary>
    /// AdministratorWindow
    /// </summary>
    public partial class AdministratorWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\..\wpf\AdministratorWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lbDesc;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\..\wpf\AdministratorWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lbKorisnik;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\..\wpf\AdministratorWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnKorisnici;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\..\wpf\AdministratorWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAerodromi;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\..\wpf\AdministratorWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnLetovi;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\..\wpf\AdministratorWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAviokompanije;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\..\wpf\AdministratorWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnSedista;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\..\wpf\AdministratorWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAvioni;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\..\wpf\AdministratorWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnKarte;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\..\wpf\AdministratorWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnClose;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/POP2018;component/wpf/administratorwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\wpf\AdministratorWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.lbDesc = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 2:
            this.lbKorisnik = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.btnKorisnici = ((System.Windows.Controls.Button)(target));
            
            #line 13 "..\..\..\wpf\AdministratorWindow.xaml"
            this.btnKorisnici.Click += new System.Windows.RoutedEventHandler(this.btnKorisnici_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.btnAerodromi = ((System.Windows.Controls.Button)(target));
            
            #line 14 "..\..\..\wpf\AdministratorWindow.xaml"
            this.btnAerodromi.Click += new System.Windows.RoutedEventHandler(this.btnAerodromi_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.btnLetovi = ((System.Windows.Controls.Button)(target));
            
            #line 15 "..\..\..\wpf\AdministratorWindow.xaml"
            this.btnLetovi.Click += new System.Windows.RoutedEventHandler(this.btnLetovi_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.btnAviokompanije = ((System.Windows.Controls.Button)(target));
            
            #line 16 "..\..\..\wpf\AdministratorWindow.xaml"
            this.btnAviokompanije.Click += new System.Windows.RoutedEventHandler(this.btnAviokompanije_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.btnSedista = ((System.Windows.Controls.Button)(target));
            
            #line 17 "..\..\..\wpf\AdministratorWindow.xaml"
            this.btnSedista.Click += new System.Windows.RoutedEventHandler(this.btnSedista_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.btnAvioni = ((System.Windows.Controls.Button)(target));
            
            #line 18 "..\..\..\wpf\AdministratorWindow.xaml"
            this.btnAvioni.Click += new System.Windows.RoutedEventHandler(this.btnAvioni_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.btnKarte = ((System.Windows.Controls.Button)(target));
            
            #line 19 "..\..\..\wpf\AdministratorWindow.xaml"
            this.btnKarte.Click += new System.Windows.RoutedEventHandler(this.btnKarte_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.btnClose = ((System.Windows.Controls.Button)(target));
            
            #line 22 "..\..\..\wpf\AdministratorWindow.xaml"
            this.btnClose.Click += new System.Windows.RoutedEventHandler(this.btnClose_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

