﻿#pragma checksum "..\..\..\wpf\KarteWindow.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "44AA1C6024F28840D6CE475E5BEDC2B417FEA7E3"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using POP2018.wpf;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace POP2018.wpf {
    
    
    /// <summary>
    /// KarteWindow
    /// </summary>
    public partial class KarteWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\..\wpf\KarteWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dgKarte;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\..\wpf\KarteWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnDelete;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\..\wpf\KarteWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnClose;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\..\wpf\KarteWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAdd;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\..\wpf\KarteWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnUpdate;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\..\wpf\KarteWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lbPretragaBrojLeta;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\..\wpf\KarteWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbPretragaBrojLeta;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\..\wpf\KarteWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lbPretragaPutnik;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\..\wpf\KarteWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbPretragaPutnik;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\..\wpf\KarteWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lbPretragaPolaziste;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\..\wpf\KarteWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbPretragaPolaziste;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\..\wpf\KarteWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lbPretragaDestinacija;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\..\wpf\KarteWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbPretragaDestinacija;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\..\wpf\KarteWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lbPretragaKlasa;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\..\wpf\KarteWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbPretragaKlasa;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/POP2018;component/wpf/kartewindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\wpf\KarteWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.dgKarte = ((System.Windows.Controls.DataGrid)(target));
            
            #line 10 "..\..\..\wpf\KarteWindow.xaml"
            this.dgKarte.AutoGeneratingColumn += new System.EventHandler<System.Windows.Controls.DataGridAutoGeneratingColumnEventArgs>(this.dgKarte_AutoGeneratingColumn);
            
            #line default
            #line hidden
            return;
            case 2:
            this.btnDelete = ((System.Windows.Controls.Button)(target));
            
            #line 11 "..\..\..\wpf\KarteWindow.xaml"
            this.btnDelete.Click += new System.Windows.RoutedEventHandler(this.btnDelete_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.btnClose = ((System.Windows.Controls.Button)(target));
            
            #line 12 "..\..\..\wpf\KarteWindow.xaml"
            this.btnClose.Click += new System.Windows.RoutedEventHandler(this.btnClose_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.btnAdd = ((System.Windows.Controls.Button)(target));
            
            #line 13 "..\..\..\wpf\KarteWindow.xaml"
            this.btnAdd.Click += new System.Windows.RoutedEventHandler(this.btnAdd_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.btnUpdate = ((System.Windows.Controls.Button)(target));
            
            #line 14 "..\..\..\wpf\KarteWindow.xaml"
            this.btnUpdate.Click += new System.Windows.RoutedEventHandler(this.btnUpdate_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.lbPretragaBrojLeta = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 7:
            this.tbPretragaBrojLeta = ((System.Windows.Controls.TextBox)(target));
            
            #line 16 "..\..\..\wpf\KarteWindow.xaml"
            this.tbPretragaBrojLeta.KeyUp += new System.Windows.Input.KeyEventHandler(this.tbPretragaBrojLeta_KeyUp);
            
            #line default
            #line hidden
            return;
            case 8:
            this.lbPretragaPutnik = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 9:
            this.tbPretragaPutnik = ((System.Windows.Controls.TextBox)(target));
            
            #line 18 "..\..\..\wpf\KarteWindow.xaml"
            this.tbPretragaPutnik.KeyUp += new System.Windows.Input.KeyEventHandler(this.tbPretragaPutnik_KeyUp);
            
            #line default
            #line hidden
            return;
            case 10:
            this.lbPretragaPolaziste = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 11:
            this.tbPretragaPolaziste = ((System.Windows.Controls.TextBox)(target));
            
            #line 20 "..\..\..\wpf\KarteWindow.xaml"
            this.tbPretragaPolaziste.KeyUp += new System.Windows.Input.KeyEventHandler(this.tbPretragaPolaziste_KeyUp);
            
            #line default
            #line hidden
            return;
            case 12:
            this.lbPretragaDestinacija = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 13:
            this.tbPretragaDestinacija = ((System.Windows.Controls.TextBox)(target));
            
            #line 22 "..\..\..\wpf\KarteWindow.xaml"
            this.tbPretragaDestinacija.KeyUp += new System.Windows.Input.KeyEventHandler(this.tbPretragaDestinacija_KeyUp);
            
            #line default
            #line hidden
            return;
            case 14:
            this.lbPretragaKlasa = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 15:
            this.tbPretragaKlasa = ((System.Windows.Controls.TextBox)(target));
            
            #line 24 "..\..\..\wpf\KarteWindow.xaml"
            this.tbPretragaKlasa.KeyUp += new System.Windows.Input.KeyEventHandler(this.tbPretragaKlasa_KeyUp);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

