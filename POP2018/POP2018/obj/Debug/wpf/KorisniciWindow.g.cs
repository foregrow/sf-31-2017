﻿#pragma checksum "..\..\..\wpf\KorisniciWindow.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "C89808075D685196052CC5160EE3B439F108B4ED"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using POP2018.wpf;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace POP2018.wpf {
    
    
    /// <summary>
    /// KorisniciWindow
    /// </summary>
    public partial class KorisniciWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\..\wpf\KorisniciWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dgKorisnici;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\..\wpf\KorisniciWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnDelete;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\..\wpf\KorisniciWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnClose;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\..\wpf\KorisniciWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAdd;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\..\wpf\KorisniciWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnUpdate;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\..\wpf\KorisniciWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lbPretragaKorisnicko;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\..\wpf\KorisniciWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbPretragaKorisnicko;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\..\wpf\KorisniciWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lbPretragaIme;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\..\wpf\KorisniciWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbPretragaIme;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\..\wpf\KorisniciWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lbPretragaPrezime;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\..\wpf\KorisniciWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbPretragaPrezime;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\..\wpf\KorisniciWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lbPretragaTip;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\..\wpf\KorisniciWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbPretragaTip;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/POP2018;component/wpf/korisniciwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\wpf\KorisniciWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.dgKorisnici = ((System.Windows.Controls.DataGrid)(target));
            
            #line 10 "..\..\..\wpf\KorisniciWindow.xaml"
            this.dgKorisnici.AutoGeneratingColumn += new System.EventHandler<System.Windows.Controls.DataGridAutoGeneratingColumnEventArgs>(this.dgKorisnici_AutoGeneratingColumn);
            
            #line default
            #line hidden
            return;
            case 2:
            this.btnDelete = ((System.Windows.Controls.Button)(target));
            
            #line 11 "..\..\..\wpf\KorisniciWindow.xaml"
            this.btnDelete.Click += new System.Windows.RoutedEventHandler(this.btnDelete_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.btnClose = ((System.Windows.Controls.Button)(target));
            
            #line 12 "..\..\..\wpf\KorisniciWindow.xaml"
            this.btnClose.Click += new System.Windows.RoutedEventHandler(this.btnClose_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.btnAdd = ((System.Windows.Controls.Button)(target));
            
            #line 13 "..\..\..\wpf\KorisniciWindow.xaml"
            this.btnAdd.Click += new System.Windows.RoutedEventHandler(this.btnAdd_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.btnUpdate = ((System.Windows.Controls.Button)(target));
            
            #line 14 "..\..\..\wpf\KorisniciWindow.xaml"
            this.btnUpdate.Click += new System.Windows.RoutedEventHandler(this.btnUpdate_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.lbPretragaKorisnicko = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 7:
            this.tbPretragaKorisnicko = ((System.Windows.Controls.TextBox)(target));
            
            #line 16 "..\..\..\wpf\KorisniciWindow.xaml"
            this.tbPretragaKorisnicko.KeyUp += new System.Windows.Input.KeyEventHandler(this.tbPretragaKorisnicko_KeyUp);
            
            #line default
            #line hidden
            return;
            case 8:
            this.lbPretragaIme = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 9:
            this.tbPretragaIme = ((System.Windows.Controls.TextBox)(target));
            
            #line 18 "..\..\..\wpf\KorisniciWindow.xaml"
            this.tbPretragaIme.KeyUp += new System.Windows.Input.KeyEventHandler(this.tbPretragaIme_KeyUp);
            
            #line default
            #line hidden
            return;
            case 10:
            this.lbPretragaPrezime = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 11:
            this.tbPretragaPrezime = ((System.Windows.Controls.TextBox)(target));
            
            #line 20 "..\..\..\wpf\KorisniciWindow.xaml"
            this.tbPretragaPrezime.KeyUp += new System.Windows.Input.KeyEventHandler(this.tbPretragaPrezime_KeyUp);
            
            #line default
            #line hidden
            return;
            case 12:
            this.lbPretragaTip = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 13:
            this.tbPretragaTip = ((System.Windows.Controls.TextBox)(target));
            
            #line 22 "..\..\..\wpf\KorisniciWindow.xaml"
            this.tbPretragaTip.KeyUp += new System.Windows.Input.KeyEventHandler(this.tbPretragaTip_KeyUp);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

