﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace POP2018.validations
{
    class EmailValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if(value.ToString().Contains("@") && value.ToString().EndsWith(".com"))
            {
                return new ValidationResult(true,"Correct format! ");
            }
            return new ValidationResult(false, "Incorrect format! ");
        }
    }
}
