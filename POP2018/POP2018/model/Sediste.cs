﻿using POP2018.database;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POP2018.model
{
    public class Sediste : ObservableObject, ICloneable
    {

        private int id;
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private string brojRedova;
        public string BrojRedova
        {
            get { return brojRedova; }
            set { brojRedova = value; OnPropertyChanged("BrojRedova"); }
        }

        private string brojKolona;
        public string BrojKolona
        {
            get { return brojKolona; }
            set { brojKolona = value; OnPropertyChanged("BrojKolona"); }
        }



        private bool aktivan;

        public bool Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; OnPropertyChanged("Aktivan"); }
        }


        private ObservableCollection<string> listaSlobodnihSedista;

        public ObservableCollection<string> ListaSlobodnihSedista
        {
            get { return listaSlobodnihSedista; }
            set { listaSlobodnihSedista = value; OnPropertyChanged("ListaSlobodnihSedista"); }
        }


        public Sediste()
        {
            aktivan = true;
            ListaSlobodnihSedista = new ObservableCollection<string>();
        }


        public void GenerisanjeSedista()
        {
            int count = 1;
            string all = "0ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            int rows = Int32.Parse(BrojRedova);
            int columns = Int32.Parse(BrojKolona);

            int[,] arr = new int[rows, columns];

            int rowLength = arr.GetLength(0);
            int colLength = arr.GetLength(1);

            for (int i = 1; i <= rowLength; i++)
            {
                for (int j = 1; j <= colLength; j++)
                {
                    ListaSlobodnihSedista.Add(i.ToString() + all[count]);
                    ++count;
                }
                count = 1;
            }
        }

        public object Clone()
        {
            Sediste kopija = new Sediste();
            kopija.Id = Id;
            kopija.BrojRedova = BrojRedova;
            kopija.BrojKolona = BrojKolona;
            kopija.ListaSlobodnihSedista = new ObservableCollection<string>(ListaSlobodnihSedista);
            return kopija;
        }

        public static void UcitajSedista()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Aplikacija.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"SELECT * FROM Sediste";

                SqlDataAdapter daSedista = new SqlDataAdapter();
                daSedista.SelectCommand = command;

                DataSet dsSedista = new DataSet();
                daSedista.Fill(dsSedista, "Sediste");

                foreach (DataRow row in dsSedista.Tables["Sediste"].Rows)
                {
                    Sediste s = new Sediste();

                    s.Id = (int)row["Id"];
                    s.BrojRedova = (string)row["Broj_Redova"];
                    s.BrojKolona = (string)row["Broj_Kolona"];
                    s.Aktivan = (bool)row["Aktivan"];
                    s.GenerisanjeSedista();
                    Aplikacija.Instance.Sedista.Add(s);
                }
            }
        }

        public static void DodajSediste(Sediste s)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO Sediste (Broj_Redova, Broj_Kolona, Aktivan) VALUES (@Broj_Redova, @Broj_Kolona, @Aktivan)";

                command.Parameters.Add(new SqlParameter("@Broj_Redova", s.BrojRedova));
                command.Parameters.Add(new SqlParameter("@Broj_Kolona", s.BrojKolona));
                command.Parameters.Add(new SqlParameter("@Aktivan", s.Aktivan));

                command.ExecuteNonQuery();
            }
        }

        public static void IzmeniSediste(Sediste s)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"UPDATE Sediste SET Broj_Redova=@Broj_Redova, Broj_Kolona=@Broj_Kolona, Aktivan=@Aktivan WHERE Id=@Id";

                command.Parameters.Add(new SqlParameter("@Id", s.Id));
                command.Parameters.Add(new SqlParameter("@Broj_Redova", s.BrojRedova));
                command.Parameters.Add(new SqlParameter("@Broj_Kolona", s.BrojKolona));
                command.Parameters.Add(new SqlParameter("@Aktivan", s.Aktivan));

                command.ExecuteNonQuery();
                
            }
        }

        public static void ObrisiSediste(Sediste s)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {

                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"UPDATE Sediste SET Aktivan=@Aktivan WHERE Id=@Id";

                command.Parameters.Add(new SqlParameter("@Id", s.Id));
                command.Parameters.Add(new SqlParameter("@Broj_Redova", s.BrojRedova));
                command.Parameters.Add(new SqlParameter("@Broj_Kolona", s.BrojKolona));
                command.Parameters.Add(new SqlParameter("@Aktivan", s.Aktivan));

                command.ExecuteNonQuery();
            }
        }
    }

}
