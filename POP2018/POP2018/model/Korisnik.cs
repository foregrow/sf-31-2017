﻿using POP2018.database;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POP2018.model
{
    public class Korisnik : ObservableObject, ICloneable
    {

        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private string korisnickoIme;
        private string lozinka;
        private string ime;
        private string prezime;
        private string adresa;
        private string email;
        private bool aktivan;
        public enum EPol { M, Z }
        private EPol pol;
        public enum ETipKorisnika { ADMINISTRATOR, PUTNIK, NEPRIJAVLJEN }
        private ETipKorisnika tipKorisnika;

        public Korisnik() { aktivan = true; tipKorisnika = ETipKorisnika.NEPRIJAVLJEN; }

        public Korisnik(string korisnickoIme, string lozinka, string ime, string prezime, string adresa, string email,
            EPol pol, ETipKorisnika tipKorisnika = ETipKorisnika.NEPRIJAVLJEN)
        {
            aktivan = true;
            KorisnickoIme = korisnickoIme;
            Lozinka = lozinka;
            Ime = ime;
            Prezime = prezime;
            Adresa = adresa;
            Email = email;
            Aktivan = aktivan;
            Pol = pol;
            TipKorisnika = tipKorisnika;
        }

        public string KorisnickoIme
        {
            get
            {
                return korisnickoIme;
            }
            set
            {
                korisnickoIme = value;
                OnPropertyChanged("KorisnickoIme");
            }
        }

        public string Lozinka
        {
            get
            {
                return lozinka;
            }
            set
            {
                lozinka = value;
                OnPropertyChanged("Lozinka");
            }
        }

        public string Ime
        {
            get
            {
                return ime;
            }
            set
            {
                ime = value;
                OnPropertyChanged("Ime");
            }
        }

        public string Prezime
        {
            get
            {
                return prezime;
            }
            set
            {
                prezime = value;
                OnPropertyChanged("Prezime");
            }
        }

        public string Adresa
        {
            get
            {
                return adresa;
            }
            set
            {
                adresa = value;
                OnPropertyChanged("Adresa");
            }
        }

        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
                OnPropertyChanged("Email");
            }
        }

        public bool Aktivan
        {
            get
            {
                return aktivan;
            }
            set
            {
                aktivan = value;
                OnPropertyChanged("Aktivan");
            }
        }

        public ETipKorisnika TipKorisnika
        {
            get
            {
                return tipKorisnika;
            }
            set
            {
                tipKorisnika = value;
                OnPropertyChanged("TipKorisnika");
            }
        }

        public EPol Pol
        {
            get
            {

                return pol;
            }
            set
            {
                pol = value;
                OnPropertyChanged("Pol");
            }
        }

        public object Clone()
        {
            Korisnik kopija = new Korisnik();
            kopija.Id = Id;
            kopija.KorisnickoIme = KorisnickoIme;
            kopija.Lozinka = Lozinka;
            kopija.Ime = Ime;
            kopija.Prezime = Prezime;
            kopija.Adresa = Adresa;
            kopija.Email = Email;
            kopija.Aktivan = Aktivan;
            kopija.TipKorisnika = TipKorisnika;
            kopija.Pol = Pol;
            return kopija;
        }

        public override string ToString()
        {
            return $"Korisnicko ime: {KorisnickoIme}, Ime: {Ime}, Prezime: {Prezime}, Adresa: {Adresa}, Email: {Email}, Tip korisnika: {TipKorisnika}";
        }


        public static void UcitajKorisnike()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Aplikacija.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"SELECT * FROM Korisnik";

                SqlDataAdapter daKor = new SqlDataAdapter();
                daKor.SelectCommand = command;

                DataSet dsKor = new DataSet();
                daKor.Fill(dsKor, "Korisnik");

                foreach (DataRow row in dsKor.Tables["Korisnik"].Rows)
                {
                    Korisnik kor = new Korisnik();
                    kor.Id = (int)row["Id"];
                    kor.KorisnickoIme = (string)row["Korisnicko_Ime"];
                    kor.Lozinka = (string)row["Lozinka"];
                    kor.Ime = (string)row["Ime"];
                    kor.Prezime = (string)row["Prezime"];
                    kor.Adresa = (string)row["Adresa"];
                    kor.Email = (string)row["Email"];
                    kor.Aktivan = (bool)row["Aktivan"];
                    kor.Pol = (EPol)Enum.Parse(typeof(EPol), row["Pol"].ToString());
                    kor.TipKorisnika = (ETipKorisnika)Enum.Parse(typeof(ETipKorisnika), row["Tip_Korisnika"].ToString());

                    Aplikacija.Instance.Korisnici.Add(kor);
                }
            }
        }

        public static void DodajKorisnika(Korisnik k)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO Korisnik (Korisnicko_Ime, Lozinka, Ime, Prezime, Adresa, Email, Aktivan, Pol, Tip_Korisnika) VALUES (@Korisnicko_Ime, @Lozinka, @Ime, @Prezime, @Adresa, @Email, @Aktivan, @Pol, @Tip_Korisnika)";

                command.Parameters.Add(new SqlParameter("@Korisnicko_Ime", k.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("@Lozinka", k.Lozinka));
                command.Parameters.Add(new SqlParameter("@Ime", k.Ime));
                command.Parameters.Add(new SqlParameter("@Prezime", k.Prezime));
                command.Parameters.Add(new SqlParameter("@Adresa", k.Adresa));
                command.Parameters.Add(new SqlParameter("@Email", k.Email));
                command.Parameters.Add(new SqlParameter("@Aktivan", k.Aktivan));
                command.Parameters.Add(new SqlParameter("@Pol", k.Pol));
                command.Parameters.Add(new SqlParameter("@Tip_Korisnika", k.TipKorisnika));

                command.ExecuteNonQuery();
            }
        }

        public static void IzmeniKorisnika(Korisnik k)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {

                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"UPDATE Korisnik SET Korisnicko_Ime=@Korisnicko_Ime, Lozinka=@Lozinka, Ime=@Ime, Prezime=@Prezime, Adresa=@Adresa, Email=@Email, Aktivan=@Aktivan, Pol=@Pol, Tip_Korisnika=@Tip_Korisnika WHERE Id=@Id";

                command.Parameters.Add(new SqlParameter("@Id", k.Id));
                command.Parameters.Add(new SqlParameter("@Korisnicko_Ime", k.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("@Lozinka", k.Lozinka));
                command.Parameters.Add(new SqlParameter("@Ime", k.Ime));
                command.Parameters.Add(new SqlParameter("@Prezime", k.Prezime));
                command.Parameters.Add(new SqlParameter("@Adresa", k.Adresa));
                command.Parameters.Add(new SqlParameter("@Email", k.Email));
                command.Parameters.Add(new SqlParameter("@Aktivan", k.Aktivan));
                command.Parameters.Add(new SqlParameter("@Pol", k.Pol));
                command.Parameters.Add(new SqlParameter("@Tip_Korisnika", k.TipKorisnika));

                command.ExecuteNonQuery();
            }
        }

        public static void BrisanjeKorisnika(Korisnik k)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {

                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"UPDATE Korisnik SET Aktivan=@Aktivan WHERE Id=@Id";

                command.Parameters.Add(new SqlParameter("@Id", k.Id));
                command.Parameters.Add(new SqlParameter("@Korisnicko_Ime", k.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("@Lozinka", k.Lozinka));
                command.Parameters.Add(new SqlParameter("@Ime", k.Ime));
                command.Parameters.Add(new SqlParameter("@Prezime", k.Prezime));
                command.Parameters.Add(new SqlParameter("@Adresa", k.Adresa));
                command.Parameters.Add(new SqlParameter("@Email", k.Email));
                command.Parameters.Add(new SqlParameter("@Aktivan", k.Aktivan));
                command.Parameters.Add(new SqlParameter("@Pol", k.Pol));
                command.Parameters.Add(new SqlParameter("@Tip_Korisnika", k.TipKorisnika));

                command.ExecuteNonQuery();
            }
        }

    }
}
