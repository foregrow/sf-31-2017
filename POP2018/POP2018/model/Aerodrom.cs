﻿using POP2018.database;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POP2018.model
{
    public class Aerodrom : ObservableObject, ICloneable
    {
        private string sifra;
        private string naziv;
        private string grad;
        private bool aktivan;

        public Aerodrom() { aktivan = true; }

        public Aerodrom(string sifra, string naziv, string grad)
        {
            aktivan = true;
            Sifra = sifra;
            Naziv = naziv;
            Grad = grad;
            Aktivan = aktivan;
        }

        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }


        public string Sifra
        {
            get
            {
                return sifra;
            }
            set
            {
                sifra = value;
                OnPropertyChanged("Sifra");
            }
        }

        public string Naziv
        {
            get
            {
                return naziv;
            }
            set
            {
                naziv = value;
                OnPropertyChanged("Naziv");
            }
        }

        public string Grad
        {
            get
            {
                return grad;
            }
            set
            {
                grad = value;
                OnPropertyChanged("Grad");
            }
        }

        public bool Aktivan
        {
            get
            {
                return aktivan;
            }
            set
            {
                aktivan = value;
                OnPropertyChanged("Aktivan");
            }
        }

        public object Clone()
        {
            Aerodrom kopija = new Aerodrom();
            kopija.Id = Id;
            kopija.Sifra = Sifra;
            kopija.Naziv = Naziv;
            kopija.Grad = Grad;
            kopija.Aktivan = Aktivan;
            return kopija;
        }

        public override string ToString()
        {
            return $"Sifra {Sifra}, Naziv {Naziv}, Grad {Grad}";
        }

        public static void UcitajAerodrome()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Aplikacija.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"SELECT * FROM Aerodrom";

                SqlDataAdapter daAerodromi = new SqlDataAdapter();
                daAerodromi.SelectCommand = command;

                DataSet dsAerodromi = new DataSet();
                daAerodromi.Fill(dsAerodromi, "Aerodrom");

                foreach (DataRow row in dsAerodromi.Tables["Aerodrom"].Rows)
                {
                    Aerodrom aero = new Aerodrom();

                    aero.Id = (int)row["Id"];
                    aero.Sifra = (string)row["Sifra"];
                    aero.Naziv = (string)row["Naziv"];
                    aero.Grad = (string)row["Grad"];
                    aero.Aktivan = (bool)row["Aktivan"];

                    Aplikacija.Instance.Aerodromi.Add(aero);
                }
            }
        }

        public static void DodajAerodrom(Aerodrom a)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO Aerodrom (Sifra, Naziv, Grad, Aktivan) VALUES (@Sifra, @Naziv, @Grad, @Aktivan)";

                command.Parameters.Add(new SqlParameter("@Sifra", a.Sifra));
                command.Parameters.Add(new SqlParameter("@Naziv", a.Naziv));
                command.Parameters.Add(new SqlParameter("@Grad", a.Grad));
                command.Parameters.Add(new SqlParameter("@Aktivan", a.Aktivan));

                command.ExecuteNonQuery();
            }
        }

        public static void IzmeniAerodrom(Aerodrom a)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                if (!a.Sifra.Equals(String.Empty))//ako postoji u bazi
                {
                    conn.Open();

                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = @"UPDATE Aerodrom SET Sifra=@Sifra, Naziv=@Naziv, Grad=@Grad, Aktivan=@Aktivan WHERE Sifra=@Sifra";

                    //command.Parameters.Add(new SqlParameter("@Id", a.Id));
                    command.Parameters.Add(new SqlParameter("@Sifra", a.Sifra));
                    command.Parameters.Add(new SqlParameter("@Naziv", a.Naziv));
                    command.Parameters.Add(new SqlParameter("@Grad", a.Grad));
                    command.Parameters.Add(new SqlParameter("@Aktivan", a.Aktivan));

                    command.ExecuteNonQuery();
                }
            }
        }

        public static void BrisanjeAerodroma(Aerodrom a)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                if (!a.Sifra.Equals(String.Empty))
                {
                    conn.Open();

                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = @"UPDATE Aerodrom SET Aktivan=@Aktivan WHERE Sifra=@Sifra";

                    //command.Parameters.Add(new SqlParameter("@Id", a.Id));
                    command.Parameters.Add(new SqlParameter("@Sifra", a.Sifra));
                    command.Parameters.Add(new SqlParameter("@Naziv", a.Naziv));
                    command.Parameters.Add(new SqlParameter("@Grad", a.Grad));
                    command.Parameters.Add(new SqlParameter("@Aktivan", a.Aktivan));

                    command.ExecuteNonQuery();
                }
            }
        }
    }
    
}
