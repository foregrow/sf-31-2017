﻿using POP2018.database;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POP2018.model
{
    public class Avion : ObservableObject, ICloneable
    {
        
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value;}
        }


        private string brojLeta;

        public string BrojLeta
        {
            get { return brojLeta; }
            set { brojLeta = value; OnPropertyChanged("BrojLeta"); }
        }

        private Sediste sedistaBiznis;

        public Sediste SedistaBiznis
        {
            get { return sedistaBiznis; }
            set { sedistaBiznis = value; OnPropertyChanged("SedistaBiznis"); }
        }

        private Sediste sedistaEkonomske;

        public Sediste SedistaEkonomske
        {
            get { return sedistaEkonomske; }
            set { sedistaEkonomske = value; OnPropertyChanged("SedistaEkonomske"); }
        }
        
        private string nazivAviokompanije;

        public string NazivAviokompanije
        {
            get { return nazivAviokompanije; }
            set { nazivAviokompanije = value; OnPropertyChanged("NazivAviokompanije"); }
        }

        private bool aktivan;

        public bool Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; OnPropertyChanged("Aktivan"); }
        }

        public Avion() {
            aktivan = true;
            SedistaBiznis = new Sediste();
            SedistaEkonomske = new Sediste();
        }

        public Avion(string brojLeta, Sediste sedistaBiznis, 
            Sediste sedistaEkonomske, string nazivAviokompanije)
        {
            aktivan = true;
            BrojLeta = brojLeta;
            SedistaBiznis = sedistaBiznis;
            SedistaEkonomske = sedistaEkonomske;
            NazivAviokompanije = nazivAviokompanije;
        }

        public object Clone()
        {
            Avion kopija = new Avion();
            kopija.Id = Id;
            kopija.BrojLeta = BrojLeta;
            kopija.SedistaBiznis = SedistaBiznis;
            kopija.SedistaEkonomske = SedistaEkonomske;
            kopija.NazivAviokompanije = NazivAviokompanije;
            kopija.Aktivan = Aktivan;
            return kopija;
        }

        public override string ToString()
        {
            return $"Broj leta: {BrojLeta}, Naziv aviokompanije: {NazivAviokompanije}";
        }


        public static void UcitajAvione()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Aplikacija.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select Id, Broj_Leta, Avio, Aktivan from Avion";

                SqlDataAdapter daAvion = new SqlDataAdapter();
                daAvion.SelectCommand = command;

                DataSet dsAvion = new DataSet();
                daAvion.Fill(dsAvion, "Avion");


                foreach (DataRow row in dsAvion.Tables["Avion"].Rows)
                {
                    Avion avion = new Avion();
                    avion.Id = (int)row["Id"];
                    avion.BrojLeta = (string)row["Broj_Leta"];
                    avion.NazivAviokompanije = (string)row["Avio"];
                    avion.Aktivan = (bool)row["Aktivan"];

                    Aplikacija.Instance.Avioni.Add(avion);
                }
            }

            foreach (Avion avi in Aplikacija.Instance.Avioni)
            {

                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = Aplikacija.CONNECTION_STRING;
                    conn.Open();

                    SqlCommand command = conn.CreateCommand();

                    command.CommandText = @"SELECT s.Id, s.Broj_Redova, s.Broj_Kolona, s.Aktivan FROM Sediste s, Avion a 
                                            WHERE s.Id=a.Sedista_Biznis and a.Id=@IdAviona";
                    command.Parameters.AddWithValue("@IdAviona", avi.Id);
                    SqlDataAdapter daSedista = new SqlDataAdapter();
                    daSedista.SelectCommand = command;

                    DataSet dsSedista = new DataSet();
                    daSedista.Fill(dsSedista, "Sediste");

                    foreach (DataRow row in dsSedista.Tables["Sediste"].Rows)
                    {
                        Sediste s = new Sediste();

                        s.Id = (int)row["Id"];
                        s.BrojRedova = (string)row["Broj_Redova"];
                        s.BrojKolona = (string)row["Broj_Kolona"];
                        s.Aktivan = (bool)row["Aktivan"];
                        s.GenerisanjeSedista();
                        avi.SedistaBiznis = s;
                    }
                
                    command = conn.CreateCommand();

                    command.CommandText = @"SELECT s.Id, s.Broj_Redova, s.Broj_Kolona, s.Aktivan FROM Sediste s, Avion a 
                                            WHERE s.Id=a.Sedista_Ekonomske and a.Id=@IdAviona";
                    command.Parameters.AddWithValue("@IdAviona", avi.Id);

                    daSedista = new SqlDataAdapter();
                    daSedista.SelectCommand = command;

                    dsSedista = new DataSet();
                    daSedista.Fill(dsSedista, "Sediste");

                    foreach (DataRow row in dsSedista.Tables["Sediste"].Rows)
                    {
                        Sediste s = new Sediste();
                        s.Id = (int)row["Id"];
                        s.BrojRedova = (string)row["Broj_Redova"];
                        s.BrojKolona = (string)row["Broj_Kolona"];
                        s.Aktivan = (bool)row["Aktivan"];
                        s.GenerisanjeSedista();

                        avi.SedistaEkonomske = s;
                    }
                }
                
            }
        }

        public static void DodajAvion(Avion a)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO Avion (Broj_Leta,Sedista_Biznis, Sedista_Ekonomske, Avio, Aktivan) 
                                        VALUES (@Broj_Leta, @Sedista_Biznis, @Sedista_Ekonomske, @Avio, @Aktivan)";

                command.Parameters.Add(new SqlParameter("@Broj_Leta", a.BrojLeta));
                command.Parameters.Add(new SqlParameter("@Sedista_Biznis", a.SedistaBiznis.Id));
                command.Parameters.Add(new SqlParameter("@Sedista_Ekonomske", a.SedistaEkonomske.Id));
                command.Parameters.Add(new SqlParameter("@Avio", a.NazivAviokompanije));
                command.Parameters.Add(new SqlParameter("@Aktivan", a.Aktivan));

                command.ExecuteNonQuery();
            }
        }

        public static void IzmeniAvion(Avion a)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {

                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"UPDATE Avion SET Broj_Leta=@Broj_Leta ,Sedista_Biznis=@Sedista_Biznis, Sedista_Ekonomske=@Sedista_Ekonomske, Avio=@Avio, Aktivan=@Aktivan WHERE Id=@Id";

                command.Parameters.Add(new SqlParameter("@Id", a.Id));
                command.Parameters.Add(new SqlParameter("@Broj_Leta", a.BrojLeta));
                command.Parameters.Add(new SqlParameter("@Sedista_Biznis", a.SedistaBiznis.Id));
                command.Parameters.Add(new SqlParameter("@Sedista_Ekonomske", a.SedistaEkonomske.Id));
                command.Parameters.Add(new SqlParameter("@Avio", a.NazivAviokompanije));
                command.Parameters.Add(new SqlParameter("@Aktivan", a.Aktivan));
                
                command.ExecuteNonQuery();
            }
        }

        public static void ObrisiAvion(Avion a)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {

                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"UPDATE Avion SET Aktivan=@Aktivan WHERE Id=@Id";

                command.Parameters.Add(new SqlParameter("@Id", a.Id));
                command.Parameters.Add(new SqlParameter("@Broj_Leta", a.BrojLeta));
                command.Parameters.Add(new SqlParameter("@Sedista_Biznis", a.SedistaBiznis.Id));
                command.Parameters.Add(new SqlParameter("@Sedista_Ekonomske", a.SedistaEkonomske.Id));
                command.Parameters.Add(new SqlParameter("@Avio", a.NazivAviokompanije));
                command.Parameters.Add(new SqlParameter("@Aktivan", a.Aktivan));

                command.ExecuteNonQuery();
            }
        }

    }
}
