﻿using POP2018.database;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POP2018.model
{
    public class Let : ObservableObject, ICloneable
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public Let()
        {
            aktivan = true;
        }

        public Let(string sifra, string pilot, string polaziste, string destinacija, DateTime vremePolaska, DateTime vremeDolaska, string avio, double cena)
        {
            aktivan = true;
            Sifra = sifra;
            Pilot = pilot;
            Polaziste = polaziste;
            Destinacija = destinacija;
            VremePolaska = vremePolaska;
            VremeDolaska = vremeDolaska;
            Avio = avio;
            CenaLeta = cena;
            Aktivan = aktivan;
        }
        private string sifra;

        public string Sifra
        {
            get { return sifra; }
            set { sifra = value; OnPropertyChanged("Sifra"); }
        }

        private string pilot;

        public string Pilot
        {
            get { return pilot; }
            set { pilot = value; OnPropertyChanged("Pilot"); }
        }


        private string polaziste;

        public string Polaziste
        {
            get { return polaziste; }
            set
            {
                polaziste = value;
                OnPropertyChanged("Polaziste");
            }
        }

        private string destinacija;

        public string Destinacija
        {
            get { return destinacija; }
            set { destinacija = value; OnPropertyChanged("Destinacija"); }
        }

        private DateTime vremePolaska;

        public DateTime VremePolaska
        {
            get { return vremePolaska; }
            set { vremePolaska = value; OnPropertyChanged("VremePolaska"); }
        }

        private DateTime vremeDolaska;

        public DateTime VremeDolaska
        {
            get { return vremeDolaska; }
            set { vremeDolaska = value; OnPropertyChanged("VremeDolaska"); }
        }

        private double cenaLeta;

        public double CenaLeta
        {
            get { return cenaLeta; }
            set { cenaLeta = value; OnPropertyChanged("Cena"); }
        }

        private string avio;

        public string Avio
        {
            get { return avio; }
            set { avio = value; }
        }


        private bool aktivan;

        public bool Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; }
        }


        public object Clone()
        {
            Let kopija = new Let();
            kopija.Id = Id;
            kopija.Sifra = Sifra;
            kopija.Pilot = Pilot;
            kopija.Polaziste = Polaziste;
            kopija.Destinacija = Destinacija;
            kopija.VremePolaska = VremePolaska;
            kopija.VremeDolaska = VremeDolaska;
            kopija.Avio = Avio;
            kopija.CenaLeta = CenaLeta;
            kopija.Aktivan = Aktivan;
            return kopija;
        }

        public override string ToString()
        {
            return $"Sifra {Sifra},Polaziste {Polaziste}, Destinacija {Destinacija}, Vreme polaska {VremePolaska}, Vreme dolaska{VremeDolaska}, Cena leta {CenaLeta}";
        }

        public static void UcitajLetove()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Aplikacija.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"SELECT * FROM Let";

                SqlDataAdapter daLet = new SqlDataAdapter();
                daLet.SelectCommand = command;

                DataSet dsLet = new DataSet();
                daLet.Fill(dsLet, "Let");

                foreach (DataRow row in dsLet.Tables["Let"].Rows)
                {
                    Let let = new Let();
                    let.Id = (int)row["Id"];
                    let.Sifra = (string)row["Sifra"];
                    let.Pilot = (string)row["Pilot"];
                    let.Polaziste = (string)row["Polaziste"];
                    let.Destinacija = (string)row["Destinacija"];
                    let.VremePolaska = (DateTime)row["Vreme_Polaska"];
                    let.VremeDolaska = (DateTime)row["Vreme_Dolaska"];
                    if((string)row["Avio"] == null)
                    {
                        let.Avio = " ";
                    }
                    else
                    {
                        let.Avio = (string)row["Avio"];
                    } 
                    
                    let.CenaLeta = Convert.ToDouble(row["Cena_Leta"]);
                    let.Aktivan = (bool)row["Aktivan"];

                    Aplikacija.Instance.Letovi.Add(let);
                }
            }
        }

        public static void DodajLet(Let l)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO Let (Sifra, Pilot, Polaziste, Destinacija, Vreme_Polaska, Vreme_Dolaska, Avio, Cena_Leta, Aktivan) VALUES (@Sifra, @Pilot, @Polaziste, @Destinacija, @Vreme_Polaska, @Vreme_Dolaska, @Avio, @Cena_Leta, @Aktivan)";

                command.Parameters.Add(new SqlParameter("@Sifra", l.Sifra));
                command.Parameters.Add(new SqlParameter("@Pilot", l.Pilot));
                command.Parameters.Add(new SqlParameter("@Polaziste", l.Polaziste));
                command.Parameters.Add(new SqlParameter("@Destinacija", l.Destinacija));
                command.Parameters.Add(new SqlParameter("@Vreme_Polaska", l.VremePolaska));
                command.Parameters.Add(new SqlParameter("@Vreme_Dolaska", l.VremeDolaska));
                command.Parameters.Add(new SqlParameter("@Avio", l.Avio));
                command.Parameters.Add(new SqlParameter("@Cena_leta", l.CenaLeta));
                command.Parameters.Add(new SqlParameter("@Aktivan", l.Aktivan));

                command.ExecuteNonQuery();
            }
        }

        public static void IzmeniLet(Let l)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {

                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"UPDATE Let SET Sifra=@Sifra, Pilot=@Pilot, Polaziste=@Polaziste, Destinacija=@Destinacija, Vreme_Polaska=@Vreme_Polaska, Vreme_Dolaska=@Vreme_Dolaska, Avio=@Avio, Cena_leta=@Cena_leta, Aktivan=@Aktivan WHERE Sifra=@Sifra";


                command.Parameters.Add(new SqlParameter("@Sifra", l.Sifra));
                command.Parameters.Add(new SqlParameter("@Pilot", l.Pilot));
                command.Parameters.Add(new SqlParameter("@Polaziste", l.Polaziste));
                command.Parameters.Add(new SqlParameter("@Destinacija", l.Destinacija));
                command.Parameters.Add(new SqlParameter("@Vreme_Polaska", l.VremePolaska));
                command.Parameters.Add(new SqlParameter("@Vreme_Dolaska", l.VremeDolaska));
                command.Parameters.Add(new SqlParameter("@Avio", l.Avio));
                command.Parameters.Add(new SqlParameter("@Cena_leta", l.CenaLeta));
                command.Parameters.Add(new SqlParameter("@Aktivan", l.Aktivan));

                command.ExecuteNonQuery();
            }
        }

        public static void BrisanjeLeta(Let l)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {

                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"UPDATE Let SET Aktivan=@Aktivan WHERE Sifra=@Sifra";


                command.Parameters.Add(new SqlParameter("@Sifra", l.Sifra));
                command.Parameters.Add(new SqlParameter("@Pilot", l.Pilot));
                command.Parameters.Add(new SqlParameter("@Polaziste", l.Polaziste));
                command.Parameters.Add(new SqlParameter("@Destinacija", l.Destinacija));
                command.Parameters.Add(new SqlParameter("@Vreme_Polaska", l.VremePolaska));
                command.Parameters.Add(new SqlParameter("@Vreme_Dolaska", l.VremeDolaska));
                command.Parameters.Add(new SqlParameter("@Avio", l.Avio));
                command.Parameters.Add(new SqlParameter("@Cena_leta", l.CenaLeta));
                command.Parameters.Add(new SqlParameter("@Aktivan", l.Aktivan));

                command.ExecuteNonQuery();
            }
        }
    }
}
