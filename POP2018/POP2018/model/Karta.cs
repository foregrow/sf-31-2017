﻿using POP2018.database;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POP2018.model
{
    public class Karta: ObservableObject, ICloneable
    {

        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private string brojLeta;

        public string BrojLeta
        {
            get { return brojLeta; }
            set { brojLeta = value; OnPropertyChanged("BrojLeta"); }
        }

        private string sediste;

        public string Sediste
        {
            get { return sediste; }
            set { sediste = value; OnPropertyChanged("Sediste"); }
        }

        private string putnik;

        public string Putnik
        {
            get { return putnik; }
            set { putnik = value; OnPropertyChanged("Putnik"); }
        }

        public enum ETipKlase { BIZNIS, EKONOMSKA }
        private ETipKlase tipKlase;

        public ETipKlase TipKlase
        {
            get { return tipKlase; }
            set { tipKlase = value; OnPropertyChanged("TipKlase"); }
        }


        private string kapija;

        public string Kapija
        {
            get { return kapija; }
            set { kapija = value; OnPropertyChanged("Kapija"); }
        }

        private double cena;

        public double Cena
        {
            get { return cena; }
            set { cena = value; OnPropertyChanged("Cena"); }
        }

        private bool aktivan;

        public bool Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; OnPropertyChanged("Aktivan"); }
        }

        public object Clone()
        {
            
            Karta kopija = new Karta();
            kopija.Id = Id;
            kopija.BrojLeta = BrojLeta;
            kopija.Sediste = Sediste;
            kopija.Putnik = Putnik;
            kopija.TipKlase = TipKlase;
            kopija.Kapija = Kapija;
            kopija.Aktivan = Aktivan;
            return kopija;
        }

        public Karta() { aktivan = true; }


        public Karta(string brojLeta, string sediste, string putnik, ETipKlase tipKlase, string kapija, double cena)
        {
            aktivan = true;
            BrojLeta = brojLeta;
            Sediste = sediste;
            Putnik = putnik;
            TipKlase = tipKlase;
            Kapija = kapija;
            Cena = cena;
            Aktivan = aktivan;
        }

        public static void UcitajKarte()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Aplikacija.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"SELECT * FROM Karta";

                SqlDataAdapter daKar = new SqlDataAdapter();
                daKar.SelectCommand = command;

                DataSet dsKar = new DataSet();
                daKar.Fill(dsKar, "Karta");

                foreach (DataRow row in dsKar.Tables["Karta"].Rows)
                {
                    Karta karta = new Karta();
                    karta.Id = (int)row["Id"];
                    karta.BrojLeta = (string)row["Broj_Leta"];
                    karta.Sediste = (string)row["Sediste"];
                    karta.Putnik = (string)row["Putnik"];
                    karta.TipKlase = (ETipKlase)Enum.Parse(typeof(ETipKlase), row["Tip_Klase"].ToString());
                    karta.Kapija = (string)row["Kapija"];
                    karta.Cena = Double.Parse(row["Cena"].ToString());
                    karta.Aktivan = (bool)row["Aktivan"];

                    Aplikacija.Instance.Karte.Add(karta);
                }
            }
        }

        public static void DodajKartu(Karta k)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO Karta (Broj_Leta, Sediste, Putnik, Tip_Klase, Kapija, Aktivan, Cena) VALUES (@Broj_Leta, @Sediste, @Putnik, @Tip_Klase, @Kapija, @Aktivan, @Cena)";

                command.Parameters.Add(new SqlParameter("@Broj_Leta", k.BrojLeta));
                command.Parameters.Add(new SqlParameter("@Sediste", k.Sediste));
                command.Parameters.Add(new SqlParameter("@Putnik", k.Putnik));
                command.Parameters.Add(new SqlParameter("@Tip_Klase", k.TipKlase));
                command.Parameters.Add(new SqlParameter("@Kapija", k.Kapija));
                command.Parameters.Add(new SqlParameter("@Aktivan", k.Aktivan));
                command.Parameters.Add(new SqlParameter("@Cena", k.Cena));

                command.ExecuteNonQuery();
            }
        }

        public static void IzmeniKartu(Karta k)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"UPDATE Karta SET Broj_Leta=@Broj_Leta, Sediste=@Sediste, Putnik=@Putnik, Tip_Klase=@Tip_Klase, Kapija=@Kapija, Aktivan=@Aktivan, Cena=@Cena where Id=@Id";

                command.Parameters.Add(new SqlParameter("@Id", k.Id));
                command.Parameters.Add(new SqlParameter("@Broj_Leta", k.BrojLeta));
                command.Parameters.Add(new SqlParameter("@Sediste", k.Sediste));
                command.Parameters.Add(new SqlParameter("@Putnik", k.Putnik));
                command.Parameters.Add(new SqlParameter("@Tip_Klase", k.TipKlase));
                command.Parameters.Add(new SqlParameter("@Kapija", k.Kapija));
                command.Parameters.Add(new SqlParameter("@Aktivan", k.Aktivan));
                command.Parameters.Add(new SqlParameter("@Cena", k.Cena));

                command.ExecuteNonQuery();
            }
        }

        public static void ObrisiKartu(Karta k)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"UPDATE Karta SET Aktivan=@Aktivan where Id=@Id";

                command.Parameters.Add(new SqlParameter("@Id", k.Id));
                command.Parameters.Add(new SqlParameter("@Broj_Leta", k.BrojLeta));
                command.Parameters.Add(new SqlParameter("@Sediste", k.Sediste));
                command.Parameters.Add(new SqlParameter("@Putnik", k.Putnik));
                command.Parameters.Add(new SqlParameter("@Tip_Klase", k.TipKlase));
                command.Parameters.Add(new SqlParameter("@Kapija", k.Kapija));
                command.Parameters.Add(new SqlParameter("@Aktivan", k.Aktivan));
                command.Parameters.Add(new SqlParameter("@Cena", k.Cena));

                command.ExecuteNonQuery();
            }
        }
    }
}
