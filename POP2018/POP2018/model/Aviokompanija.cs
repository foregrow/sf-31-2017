﻿using POP2018.database;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POP2018.model
{
    public class Aviokompanija : ObservableObject, ICloneable
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private string sifra;

        public string Sifra
        {
            get { return sifra; }
            set { sifra = value; OnPropertyChanged("Sifra"); }
        }

        private ObservableCollection<Let> listaLetova;

        public ObservableCollection<Let> ListaLetova
        {
            get { return listaLetova; }
            set { listaLetova = value; OnPropertyChanged("ListaLetova"); }
        }

        private string polaziste;

        public string Polaziste
        {
            get { return polaziste; }
            set { polaziste = value; OnPropertyChanged("Polaziste"); }
        }

        private bool aktivan;

        public bool Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; }
        }

        public Aviokompanija()
        {
            aktivan = true;
            ListaLetova = new ObservableCollection<Let>();
        }
        public Aviokompanija(string sifra, string polaziste)
        {
            aktivan = true;
            ListaLetova = new ObservableCollection<Let>();
            Sifra = sifra;
            Polaziste = polaziste;
        }

        public object Clone()
        {
            Aviokompanija kopija = new Aviokompanija();
            kopija.Id = Id;
            kopija.Sifra = Sifra;
            kopija.ListaLetova = new ObservableCollection<Let>(ListaLetova);
            kopija.Polaziste = Polaziste;
            return kopija;
        }

        public override string ToString()
        {
            return $"Sifra {Sifra}, Polaziste {Polaziste}";
        }

        public static void UcitajAviokompanije()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Aplikacija.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from Aviokompanija";

                SqlDataAdapter daAvio = new SqlDataAdapter();
                daAvio.SelectCommand = command;

                DataSet dsAvio = new DataSet();
                daAvio.Fill(dsAvio, "Aviokompanija");


                foreach (DataRow row in dsAvio.Tables["Aviokompanija"].Rows)
                {
                    Aviokompanija avio = new Aviokompanija();
                    avio.Id = (int)row["Id"];
                    avio.Sifra = (string)row["Sifra"];
                    avio.Polaziste = (string)row["Polaziste"];
                    avio.Aktivan = (bool)row["Aktivan"];

                    Aplikacija.Instance.Aviokompanije.Add(avio);
                }
            }

            foreach (Aviokompanija avi in Aplikacija.Instance.Aviokompanije)
            {
                ObservableCollection<Let> listaLetova = new ObservableCollection<Let>();
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = Aplikacija.CONNECTION_STRING;
                    conn.Open();

                    SqlCommand command = conn.CreateCommand();

                    command.CommandText = @"select Id, Sifra, Polaziste, Destinacija, Vreme_Polaska, Vreme_Dolaska, Avio, Cena_Leta, Aktivan from Let where Avio=@a";
                    command.Parameters.AddWithValue("@a", avi.Sifra);

                    SqlDataAdapter daLet = new SqlDataAdapter();
                    daLet.SelectCommand = command;

                    DataSet dsLet = new DataSet();
                    daLet.Fill(dsLet, "Let");


                    foreach (DataRow row1 in dsLet.Tables["Let"].Rows)
                    {
                        Let let = new Let();
                        let.Id = (int)row1["Id"];
                        let.Sifra = (string)row1["Sifra"];
                        let.Polaziste = (string)row1["Polaziste"];
                        let.Destinacija = (string)row1["Destinacija"];
                        let.VremePolaska = (DateTime)row1["Vreme_Polaska"];
                        let.VremeDolaska = (DateTime)row1["Vreme_Dolaska"];
                        if ((string)row1["Avio"] == null)
                        {
                            let.Avio = " ";
                        }
                        else
                        {
                            let.Avio = (string)row1["Avio"];
                        }
                        let.CenaLeta = Convert.ToDouble(row1["Cena_Leta"]);
                        let.Aktivan = (bool)row1["Aktivan"];

                        listaLetova.Add(let);
                    } 
                }

                avi.ListaLetova = listaLetova; 
            }
        }
        
        public static void DodajAviokompaniju(Aviokompanija a)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO Aviokompanija (Sifra, Polaziste, Aktivan) VALUES (@Sifra, @Polaziste, @Aktivan)";

                command.Parameters.Add(new SqlParameter("@Sifra", a.Sifra));
                command.Parameters.Add(new SqlParameter("@Polaziste", a.Polaziste));
                command.Parameters.Add(new SqlParameter("@Aktivan", a.Aktivan));

                command.ExecuteNonQuery();
            }
        }

        public static void IzmeniAviokompaniju(Aviokompanija a)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"UPDATE Aviokompanija SET (Sifra=@Sifra, Polaziste=@Polaziste, Aktivan=@Aktivan WHERE Sifra=@Sifra)";

                command.Parameters.Add(new SqlParameter("@Sifra", a.Sifra));
                command.Parameters.Add(new SqlParameter("@Polaziste", a.Polaziste));
                command.Parameters.Add(new SqlParameter("@Aktivan", a.Aktivan));

                command.ExecuteNonQuery();
            }
        }

        public static void ObrisiAviokompaniju(Aviokompanija a)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"UPDATE Aviokompanija SET Aktivan=@Aktivan WHERE Sifra=@Sifra";

                command.Parameters.Add(new SqlParameter("@Sifra", a.Sifra));
                command.Parameters.Add(new SqlParameter("@Polaziste", a.Polaziste));
                command.Parameters.Add(new SqlParameter("@Aktivan", a.Aktivan));

                command.ExecuteNonQuery();
            }
        }

    }
}
