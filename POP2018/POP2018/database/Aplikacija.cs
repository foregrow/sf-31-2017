﻿using POP2018.model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POP2018.database
{
    class Aplikacija
    {

        public ObservableCollection<Korisnik> Korisnici { get; set; }
        public ObservableCollection<Aerodrom> Aerodromi { get; set; }
        public ObservableCollection<Let> Letovi { get; set; }
        public ObservableCollection<Aviokompanija> Aviokompanije { get; set; }
        public ObservableCollection<Avion> Avioni { get; set; }
        public ObservableCollection<Sediste> Sedista { get; set; }
        public ObservableCollection<Karta> Karte { get; set; }

        public static Korisnik UlogovanKorisnik { get; set; }

        public const string CONNECTION_STRING = @"Integrated Security=true;Initial Catalog=AvioServisDB;Data Source=(localdb)\MSSQLLocalDB;";

        private static Aplikacija instance = new Aplikacija();

        public static Aplikacija Instance
        {
            get
            {
                return instance;
            }
        }

        private Aplikacija()
        {
            Korisnici = new ObservableCollection<Korisnik>();
            Aerodromi = new ObservableCollection<Aerodrom>();
            Letovi = new ObservableCollection<Let>();
            Aviokompanije = new ObservableCollection<Aviokompanija>();
            Avioni = new ObservableCollection<Avion>();
            Sedista = new ObservableCollection<Sediste>();
            Karte = new ObservableCollection<Karta>();
            UlogovanKorisnik = new Korisnik();
            
        }

        

        private void PopuniPodatke()
        {
            Korisnik k = new Korisnik("a","a","a","a","aa","aa@gmail.com",Korisnik.EPol.M,Korisnik.ETipKorisnika.ADMINISTRATOR);
            Korisnici.Add(k);
            k = new Korisnik("b", "b", "b", "b", "bb", "bb@gmail.com", Korisnik.EPol.M, Korisnik.ETipKorisnika.PUTNIK);
            Korisnici.Add(k);

            Aerodrom a = new Aerodrom("BEG", "Nikola Tesla", "Beograd");
            Aerodromi.Add(a);
            a = new Aerodrom("AMS", "AMSTERDAM A", "Amsterdam");
            Aerodromi.Add(a);
            a = new Aerodrom("MDR", "MADRID A", "Madrid");
            Aerodromi.Add(a);
            a = new Aerodrom("LON", "LONDON A", "London");
            Aerodromi.Add(a);

            Aviokompanija avio = new Aviokompanija("AIS Airlines", "AMS");
            Let let = new Let("100", "", "AMS", "BEG", new DateTime(2018, 12, 3, 11, 00, 00), new DateTime(2018, 12, 3, 11, 02, 00), "AIS Airlines", 500);
            Letovi.Add(let);
            avio.ListaLetova.Add(let);
            Aviokompanije.Add(avio);

            avio = new Aviokompanija("AirTanker", "LON");
            let = new Let("101", "", "LON", "AMS", new DateTime(2018, 12, 4, 11, 00, 00), new DateTime(2018, 12, 4, 11, 02, 00), "Air Tanker", 500);
            Letovi.Add(let);
            avio.ListaLetova.Add(let);
            Aviokompanije.Add(avio);

            avio = new Aviokompanija("Air Europa", "MDR");
            let = new Let("102", "", "MDR", "BEG", new DateTime(2018, 12, 5, 11, 00, 00), new DateTime(2018, 12, 5, 11, 02, 00), "Air Europa", 500);
            Letovi.Add(let);
            avio.ListaLetova.Add(let);
            Aviokompanije.Add(avio);

            avio = new Aviokompanija("Air Serbia", "BEG");
            let = new Let("103", "", "BEG", "AMS", new DateTime(2018, 12, 2, 11, 00, 00), new DateTime(2018, 12, 2, 11, 02, 00), "Air Serbia", 500);
            Letovi.Add(let);
            avio.ListaLetova.Add(let);
            let = new Let("104", "", "BEG", "MDR", new DateTime(2018, 12, 3, 11, 00, 00), new DateTime(2018, 12, 3, 11, 02, 00), "Air Serbia", 400);
            Letovi.Add(let);
            avio.ListaLetova.Add(let);
            Aviokompanije.Add(avio);

            let = new Let("105", "", "MDR", "AMS", new DateTime(2018, 12, 5, 11, 00, 00), new DateTime(2018, 12, 5, 11, 02, 00), "Air Europa", 320);
            Letovi.Add(let);
            let = new Let("106", "", "LON", "MDR", new DateTime(2018, 12, 5, 11, 00, 00), new DateTime(2018, 12, 5, 11, 02, 00), "Air Tanker", 333);
            Letovi.Add(let);
            let = new Let("107", "", "AMS", "MDR", new DateTime(2018, 12, 5, 11, 00, 00), new DateTime(2018, 12, 5, 11, 02, 00), "AIS Airlines", 250);
            Letovi.Add(let);

            

            Sediste s = new Sediste();
            s.BrojRedova = "5";
            s.BrojKolona = "5";
            s.GenerisanjeSedista();
            Sedista.Add(s);

            Sediste s1 = new Sediste();
            s1.BrojRedova = "3";
            s1.BrojKolona = "3";
            s1.GenerisanjeSedista();
            Sedista.Add(s1);

            Sediste s2 = new Sediste();
            s2.BrojRedova = "6";
            s2.BrojKolona = "5";
            s2.GenerisanjeSedista();
            Sedista.Add(s2);

            Avion avion = new Avion("100", s2, s, "AIS Airlines");
            Avioni.Add(avion);
            avion = new Avion("101", s2, s, "Air Tanker");
            Avioni.Add(avion);
            avion = new Avion("102", s, s, "Air Europa");
            Avioni.Add(avion);
            avion = new Avion("103", s1, s, "Air Serbia");
            Avioni.Add(avion);
            avion = new Avion( "104", s1, s1, "Air Serbia");
            Avioni.Add(avion);
            avion = new Avion("105", s1, s2, "Air Europa");
            Avioni.Add(avion);
            avion = new Avion("106", s2, s1, "Air Tanker");
            Avioni.Add(avion);
            avion = new Avion( "107", s, s, "AIS Airlines");
            Avioni.Add(avion);

            Karta karta = new Karta("100", "1A", "Jimmy123", Karta.ETipKlase.BIZNIS, "KAPIJA", 750);
            Karte.Add(karta);
            karta = new Karta("101", "1A", "b", Karta.ETipKlase.BIZNIS, "KAPIJA", 750);
            Karte.Add(karta);
            karta = new Karta("102", "1B", "b", Karta.ETipKlase.EKONOMSKA, "KAPIJA", 750);
            Karte.Add(karta);
        }
        /*
        
        AERODROM:
        CREATE TABLE [dbo].[Aerodrom] (
        [Id]      INT          IDENTITY (1, 1) NOT NULL,
        [Sifra]   CHAR (3)     NOT NULL,
        [Naziv]   VARCHAR (50) NOT NULL,
        [Grad]    VARCHAR (50) NOT NULL,
        [Aktivan] BIT          NOT NULL,
        PRIMARY KEY CLUSTERED ([Id] ASC)
        );

        AVIO:
        CREATE TABLE [dbo].[Aviokompanija] (
        [Id]        INT          IDENTITY (1, 1) NOT NULL,
        [Sifra]     VARCHAR (50) NOT NULL,
        [Polaziste] VARCHAR (3)  NOT NULL,
        [Aktivan]   BIT          NOT NULL,
        PRIMARY KEY CLUSTERED ([Id] ASC)
        );

        AVION:
        CREATE TABLE [dbo].[Avion] (
        [Id]                INT          IDENTITY (1, 1) NOT NULL,
        [Pilot]             VARCHAR (50) NOT NULL,
        [Broj_Leta]         VARCHAR (3)  NOT NULL,
        [Sedista_Biznis]    INT          NOT NULL,
        [Sedista_Ekonomske] INT          NOT NULL,
        [Avio]              VARCHAR (50) NOT NULL,
        [Aktivan]           BIT          NOT NULL,
        PRIMARY KEY CLUSTERED ([Id] ASC),
        FOREIGN KEY ([Sedista_Biznis]) REFERENCES [dbo].[Sediste] ([Id]),
        FOREIGN KEY ([Sedista_Ekonomske]) REFERENCES [dbo].[Sediste] ([Id])
        );

        KARTA:
        CREATE TABLE [dbo].[Karta] (
        [Id]        INT          IDENTITY (1, 1) NOT NULL,
        [Broj_Leta] VARCHAR (3)  NOT NULL,
        [Sediste]   VARCHAR (2)  NOT NULL,
        [Putnik]    VARCHAR (50) NOT NULL,
        [Tip_Klase] VARCHAR (50) NOT NULL,
        [Kapija]    VARCHAR (50) NOT NULL,
        [Aktivan]   BIT          NOT NULL,
        [Cena]      MONEY        NOT NULL,
        PRIMARY KEY CLUSTERED ([Id] ASC)
        );

        KORISNIK:
        CREATE TABLE [dbo].[Korisnik] (
        [Id]             INT          IDENTITY (1, 1) NOT NULL,
        [Korisnicko_Ime] VARCHAR (50) NOT NULL,
        [Lozinka]        VARCHAR (50) NOT NULL,
        [Ime]            VARCHAR (50) NOT NULL,
        [Prezime]        VARCHAR (50) NOT NULL,
        [Adresa]         VARCHAR (50) NOT NULL,
        [Email]          VARCHAR (50) NOT NULL,
        [Aktivan]        BIT          NOT NULL,
        [Pol]            VARCHAR (1)  NOT NULL,
        [Tip_Korisnika]  VARCHAR (20) NOT NULL,
        PRIMARY KEY CLUSTERED ([Id] ASC)
        );

        LET:
        CREATE TABLE [dbo].[Let] (
        [Id]            INT          IDENTITY (1, 1) NOT NULL,
        [Sifra]         CHAR (3)     NOT NULL,
        [Polaziste]     CHAR (3)     NOT NULL,
        [Destinacija]   CHAR (3)     NOT NULL,
        [Vreme_Polaska] DATETIME     NOT NULL,
        [Vreme_Dolaska] DATETIME     NOT NULL,
        [Avio]          VARCHAR (30) NULL,
        [Cena_Leta]     MONEY        NOT NULL,
        [Aktivan]       BIT          NOT NULL,
        PRIMARY KEY CLUSTERED ([Id] ASC)
        );

        SEDISTE:
        CREATE TABLE [dbo].[Sediste] (
        [Id]          INT          IDENTITY (1, 1) NOT NULL,
        [Broj_Redova] VARCHAR (50) NOT NULL,
        [Broj_Kolona] VARCHAR (50) NOT NULL,
        [Aktivan]     BIT          NOT NULL,
        PRIMARY KEY CLUSTERED ([Id] ASC)
        );
        */

    }

}
